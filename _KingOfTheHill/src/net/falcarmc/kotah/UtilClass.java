package net.falcarmc.kotah;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;

public class UtilClass
{
	public static List<Material> interactables = Arrays.asList(Material.ACACIA_DOOR, Material.ACACIA_FENCE_GATE, Material.BED, Material.BIRCH_DOOR,
			Material.BIRCH_FENCE_GATE, Material.BOAT, Material.DARK_OAK_DOOR, Material.DARK_OAK_FENCE_GATE, Material.DAYLIGHT_DETECTOR, Material.DAYLIGHT_DETECTOR_INVERTED,
			Material.FENCE_GATE, Material.HOPPER, Material.HOPPER_MINECART, Material.ITEM_FRAME, Material.JUNGLE_DOOR, Material.JUNGLE_FENCE_GATE, Material.LEVER, Material.MINECART,
			Material.NOTE_BLOCK, Material.POWERED_MINECART, Material.REDSTONE_COMPARATOR, Material.REDSTONE_COMPARATOR_OFF, Material.REDSTONE_COMPARATOR_ON, Material.SIGN, Material.SIGN_POST,
			Material.STORAGE_MINECART, Material.TRAP_DOOR, Material.WALL_SIGN, Material.WOOD_BUTTON, Material.WOOD_DOOR);

	public static List<Material> containers = Arrays.asList(Material.CHEST, Material.BREWING_STAND, Material.TRAPPED_CHEST, Material.FURNACE, Material.ENDER_CHEST, Material.ENCHANTMENT_TABLE,
			Material.DISPENSER, Material.DROPPER, Material.ANVIL, Material.BEACON, Material.COMMAND, Material.WORKBENCH);

	public static boolean isInteractable(Material type)
	{
		return interactables.contains(type) || containers.contains(type);
	}
	
	public static boolean isContainer(Material type)
	{
		return containers.contains(type);
	}
	
	public static boolean isSolelyInteractable(Material type)
	{
		return interactables.contains(type);
	}
	
	public static String listOfPlayers(Collection<Player> players)
	{
		StringBuilder builder = new StringBuilder();
		int index = 0;
		for(Player player : players)
		{
			builder.append((index != 0 ? (index != players.size() - 1 ? ", " : " and ") : "") + player.getName());
		}
		return builder.toString();
	}
}
