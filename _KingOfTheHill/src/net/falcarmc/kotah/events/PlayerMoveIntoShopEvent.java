package net.falcarmc.kotah.events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveIntoShopEvent extends PlayerMoveEvent
{
	public PlayerMoveIntoShopEvent(Player player, Location from, Location to)
	{
		super(player, from, to);
	}
}
