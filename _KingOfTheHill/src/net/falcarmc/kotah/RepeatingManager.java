package net.falcarmc.kotah;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * This runs every 20 ticks
 */
public class RepeatingManager extends BukkitRunnable
{
	int seconds = 1;
	
	@Override
	public void run()
	{
		Main.getInstance().getEntityManager().clearOldEntities();
		
		Main.getInstance().getGameManager().checkHole();
		
		Main.getInstance().getBlockManager().checkBlocks(System.currentTimeMillis());
		
		Main.getInstance().getUserManager().displayScoreboardToAllPlayers();
		
		if(seconds % (Main.getInstance().getRocketPackageManager().packageSeconds) == 0)
			Main.getInstance().getRocketPackageManager().setupRocketLocation();
		
//		if(seconds % 10 == 0)
//		{
//			Player king = Main.getInstance().getGameManager().getKing();
//			if(king != null)
//				Main.getInstance().getCrownManager().checkSync(king, getFrom, getTo)
//		}
		
		Main.getInstance().getUserManager().checkSpeedPotions();
		
		if(seconds % (60 * 1) == 0)
		{
			Main.getInstance().getUserManager().saveUsers();
		}
		
		seconds++;
	}
}
