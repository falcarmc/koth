package net.falcarmc.kotah.managers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.User;
import net.falcarmc.kotah.managers.CrownManager.CrownType;
import net.falcarmc.kotah.util.ItemType;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager extends MainManager
{
	private Map<String, File> configFiles = new HashMap<>();
	private Map<String, YamlConfiguration> configs = new HashMap<>();
	private int minutesSaved = 360;

	public ConfigManager()
	{
		super();
		saveConfig(Configs.DATA.getFileName());
	}

	public enum Configs
	{
		DATA("data.yml");

		private String fileName = "null";

		Configs(String fileName)
		{
			this.fileName = fileName;
		}

		public String getFileName()
		{
			return fileName;
		}
	}

	public void saveUserToConfig(User user)
	{
		FileConfiguration config = getConfig(Configs.DATA.getFileName());
		int kills = user.getKills(false);
		int deaths = user.getDeaths(false);
		int tokens = user.getTokens();
		int permKills = user.getPermKills();
		int permDeaths = user.getPermDeaths();
		List<ItemType> items = user.getItems();
		List<String> finalItems = new ArrayList<>();
		for(ItemType item : items)
			finalItems.add(item.name());
		long lastLogoutTime = System.currentTimeMillis();
		// List<String> availableItems = new ArrayList<>();
		// for (ItemType type : user.getAvailableItems())
		// availableItems.add(type.toString());
		config.set(user.getUniqueId() + ".data.crown", user.getCrownType().toString());
		config.set(user.getUniqueId() + ".data.kills", kills);
		config.set(user.getUniqueId() + ".data.deaths", deaths);
		config.set(user.getUniqueId() + ".data.tokens", tokens);
		config.set(user.getUniqueId() + ".data.permKills", permKills);
		config.set(user.getUniqueId() + ".data.permDeaths", permDeaths);
		config.set(user.getUniqueId() + ".data.lastLogout", lastLogoutTime);
		config.set(user.getUniqueId() + ".items", finalItems);

		// config.set(user.getUniqueId() + ".data.available_items", availableItems);
		saveConfig(Configs.DATA.getFileName());
	}

	public void loadUserFromConfig(User user)
	{
		FileConfiguration config = getConfig(Configs.DATA.getFileName());
		if (config.contains(user.getUniqueId().toString()))
		{
			long millis = System.currentTimeMillis();
			long lastLogoutTime = config.getLong(user.getUniqueId() + ".data.lastLogout");
			int minutesSinceLastLogout = (int) ((millis - lastLogoutTime) / 60000f);

			int kills = config.getInt(user.getUniqueId() + ".data.kills");
			int deaths = config.getInt(user.getUniqueId() + ".data.deaths");
			int tokens = config.getInt(user.getUniqueId() + ".data.tokens");
			int permKills = config.getInt(user.getUniqueId() + ".data.permKills");
			int permDeaths = config.getInt(user.getUniqueId() + ".data.permDeaths");
			List<String> items = config.getStringList(user.getUniqueId() + ".items");

			if (minutesSinceLastLogout > minutesSaved)
			{
				permKills += kills;
				permDeaths += deaths;
				kills = 0;
				deaths = 0;
				tokens = 0;
				items = new ArrayList<String>();
			}
			List<ItemType> finalItems = new ArrayList<>();
			for (String item : items)
				finalItems.add(ItemType.valueOf(item));
			String crownName = config.getString(user.getUniqueId() + ".data.crown");
			if (crownName != null)
			{
				CrownType crownType = CrownType.valueOf(crownName);
				if (crownType != null)
					user.setCrownType(crownType);
			}
			user.setKills(kills);
			user.setDeaths(deaths);
			user.setTokens(tokens);
			user.setPermKills(permKills);
			user.setPermDeaths(permDeaths);
			user.setItems(finalItems);
			saveUserToConfig(user);
		}
	}

	// public void loadAllPluginItems(User user)
	// {
	// FileConfiguration config = getConfig(Configs.DATA.getFileName());
	// List<PluginItem> pluginItems = new ArrayList<>();
	// String pathPrefix = user.getUniqueId() + ".data.items";
	// if (config.contains(pathPrefix))
	// {
	// for (String typeName : config.getConfigurationSection(pathPrefix).getKeys(false))
	// {
	// ItemType type = PluginItem.getItemTypeFromString(typeName);
	// if (type != null)
	// {
	// for (String itemNumber : config.getConfigurationSection(pathPrefix + "." + typeName).getKeys(false))
	// {
	// PluginItem item = new PluginItem(type);
	// String itemPathPrefix = pathPrefix + "." + typeName + "." + itemNumber + ".";
	// item.setCollected(config.getLong(itemPathPrefix + ".collected"));
	// item.setDuration(config.getLong(itemPathPrefix + ".duration"));
	// item.getItem().setAmount(config.getInt(itemPathPrefix + ".amount"));
	// user.addItem(item);
	// }
	// }
	// }
	// }
	// }

	// public void saveAllPluginItems(User user)
	// {
	// FileConfiguration config = getConfig(Configs.DATA.getFileName());
	// List<PluginItem> pluginItems = new ArrayList<>();
	// String pathPrefix = user.getUniqueId() + ".data.items";
	// for (PluginItem item : user.getItems())
	// {
	// if (item.isSaveable())
	// {
	// String typeName = item.getType().toString();
	// int itemNumber = config.contains(pathPrefix + "." + typeName) ? config.getConfigurationSection(pathPrefix + "." +
	// typeName).getKeys(false).size() + 1 : 1;
	// String itemPath = pathPrefix + "." + typeName + "." + itemNumber + ".";
	// config.set(itemPath + "collected", item.getCollected());
	// config.set(itemPath + "duration", item.getDuration());
	// config.set(itemPath + "amount", item.getItem().getAmount());
	// }
	// }
	// saveConfig(Configs.DATA.getFileName());
	// }

	// public PluginItem loadPluginItem(FileConfiguration config, String path)
	// {
	// PluginItem item = new PluginItem();
	// }

	public void reloadConfig(String configName)
	{
		if (configFiles.get(configName) == null)
		{
			configFiles.put(configName, new File(Main.instance.getDataFolder(), configName));
		}
		configs.put(configName, YamlConfiguration.loadConfiguration(configFiles.get(configName)));
		InputStream defConfigStream = Main.instance.getResource(configName);
		if (defConfigStream != null)
		{
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			configs.get(configName).setDefaults(defConfig);
		}
	}

	public FileConfiguration getConfig(String configName)
	{
		if (configFiles.get(configName) == null)
		{
			reloadConfig(configName);
		}
		return configs.get(configName);
	}

	public void saveConfig(String configName)
	{
		if (configs.get(configName) == null || configFiles.get(configName) == null)
		{
			return;
		}
		try
		{
			getConfig(configName).save(configFiles.get(configName));
		} catch (IOException e)
		{
			Main.instance.getLogger().log(Level.SEVERE, "Could not save config to " + configFiles.get(configName), e);
		}
	}
}
