package net.falcarmc.kotah.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.falcarmc.kotah.User;
import net.falcarmc.kotah.util.ItemType;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.TileEntitySkull;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

public class RocketPackageManager extends MainManager
{
	public List<Location> rocketLocations = new ArrayList<>();
	public int rocketIndex = -1;
	public long lastRocketTime = -1;
	public int packageSeconds = 150;
	public int maxAmmoCount = 5;

	public RocketPackageManager(World mainWorld)
	{
		super();

		// inside tunnel opening (next to stairs)
		rocketLocations.add(new Location(mainWorld, 161, 25, -64));
		// next to the top of waterfall
		rocketLocations.add(new Location(mainWorld, 170, 50, -63));
		// behind throne
		rocketLocations.add(new Location(mainWorld, 262, 41, -47));
		// at the start of brown tunnel (to the right of the painting)
		rocketLocations.add(new Location(mainWorld, 252, 32, -65));
		// at the end of the branch of the brown tunnel
		rocketLocations.add(new Location(mainWorld, 237, 33, -57));
		// inside hideout in the hill
		rocketLocations.add(new Location(mainWorld, 225, 30, -47));
		// on a rock in the pond
		rocketLocations.add(new Location(mainWorld, 170, 22, 13));
		// on top of tree
		rocketLocations.add(new Location(mainWorld, 265, 31, 22));
		// inside explosion hole on the wall
		rocketLocations.add(new Location(mainWorld, 186, 47, 47));
		// on the roof of the small hideout building
		rocketLocations.add(new Location(mainWorld, 206, 31, 12));
		// inside the hideout building
//		rocketLocations.add(new Location(mainWorld, 211, 26, 12));
		// behind a tree in the middle of the hill
//		rocketLocations.add(new Location(mainWorld, 231, 34, -28));

		clearAllPackageLocations();
		setupRocketLocation();
	}
	
	public void clearAllPackageLocations()
	{
		for(Location location : rocketLocations)
			location.getBlock().setType(Material.AIR);
	}

	public void setupRocketLocation()
	{
		lastRocketTime = System.currentTimeMillis();
		int oldIndex = rocketIndex;
		rocketIndex = random().nextInt(rocketLocations.size());
		if (oldIndex != rocketIndex)
		{
			if (oldIndex != -1)
				clearRocket(oldIndex);
			placeNewRocket(rocketIndex);
		}
	}

	public void placeNewRocket(int index)
	{
		setBeacon(rocketLocations.get(index));
	}

	public void clearRocket(int index)
	{
		Location location = rocketLocations.get(index);
		Block block = location.getBlock();
		block.setType(Material.AIR);
	}

	public boolean hasRocketLauncher(Player player)
	{
		Inventory inventory = player.getInventory();
		for (ItemStack item : inventory.getContents())
			if (isRocketLauncher(item))
				return true;
		return false;
	}

	public boolean isRocketLauncher(ItemStack item)
	{
		return item != null && item.isSimilar(getItem(ItemType.ROCKET_LAUNCHER));
	}

	private boolean nextToRocketPackage(Location location)
	{
		// if the player is standing next to the rocket package
		Location rocketLocation = rocketLocations.get(rocketIndex);
		return (Math.abs(location.getBlockX() - rocketLocation.getBlockX()) <= 1 && location.getBlockY() == rocketLocation.getBlockY() && Math.abs(location.getBlockZ() - rocketLocation.getBlockZ()) <= 1);
	}

	public void giveRocketLauncher(Player player)
	{
		getMain().getGameManager().sendTitle(player, ChatColor.WHITE, "", ChatColor.YELLOW, "You found a rocket launcher!", 30);
		player.getInventory().addItem(getItem(ItemType.ROCKET_LAUNCHER).clone());
		User user = getUser(player);
		user.setRocketLauncherAmmo(maxAmmoCount);
	}

	public void setBeacon(Location location)
	{
		Block skull = location.getBlock();
		skull.setType(Material.SKULL);
		skull.setData((byte) 1);
		TileEntitySkull skullTile = (TileEntitySkull) ((CraftWorld) skull.getWorld()).getHandle().getTileEntity(new BlockPosition(skull.getX(), skull.getY(), skull.getZ()));
		skullTile.setGameProfile(getNonPlayerProfile("http://textures.minecraft.net/texture/c6e69b1c7e69bcd49ed974f5ac36ea275efabb8c649cb2b1fe9d6ea6166ec3", "RocketParcel"));
		skull.getWorld().refreshChunk(skull.getChunk().getX(), skull.getChunk().getZ());
	}

	public static GameProfile getNonPlayerProfile(String skinURL, String randomName)
	{
		GameProfile newSkinProfile = new GameProfile(UUID.randomUUID(), randomName);
		newSkinProfile.getProperties().put("textures", new Property("textures", Base64Coder.encodeString("{textures:{SKIN:{url:\"" + skinURL + "\"}}}")));
		return newSkinProfile;
	}

	// if the player is in reach of the rocket package and wasn't in the
	// region before
	public void checkPlayerPickupRocket(Player player, Location getFrom, Location getTo)
	{
		User user = getUser(player);
		if (nextToRocketPackage(getTo) && !nextToRocketPackage(getFrom))
		{
			if (!hasRocketLauncher(player))
			{
				giveRocketLauncher(player);
			}
			else if(user.getRocketAmmo() < maxAmmoCount)
			{
				user.setRocketLauncherAmmo(maxAmmoCount);
				getMain().getGameManager().sendTitle(player, ChatColor.RESET, "", ChatColor.YELLOW, "Replenished ammo", 20);
			}
		}
	}
}
