package net.falcarmc.kotah.managers;

import java.util.HashMap;
import java.util.Map;

import net.falcarmc.kotah.util.ExplodableBlock;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockManager extends MainManager
{
	World mainWorld;
	Map<Location, ExplodableBlock> explodableBlocks = new HashMap<>();
	Material tempMaterial;
	byte tempData;
	int tempSeconds;
	float tempDistance;
	
	public BlockManager(World mainWorld)
	{
		this.mainWorld = mainWorld;
		setupBlocks();
		placeBlocks();
	}
	
	public void setupBlocks()
	{
		int x = 268;
		addBlock(x, 32, -9, Material.COBBLESTONE, (byte)0, 20, 1.5f);
		addBlock(x, 33, -7);
		addBlock(x, 33, -8);
		addBlock(x, 33, -9);
		addBlock(x, 33, -10);
		addBlock(x, 34, -7);
		addBlock(x, 34, -9);
		addBlock(x, 34, -10);
		addBlock(x, 34, -11);
		addBlock(x, 35, -7);
		addBlock(x, 35, -8);
		addBlock(x, 35, -9);
		addBlock(x, 35, -11);
		addBlock(x, 36, -7);
		addBlock(x, 36, -8);
		addBlock(x, 36, -9);
		addBlock(x, 36, -10);
		addBlock(x, 36, -11);
		addBlock(x, 37, -8);
		addBlock(x, 37, -9);
		addBlock(x, 34, -8, Material.COBBLESTONE_STAIRS, (byte)7, 20, 3f);
		addBlock(x, 35, -10, Material.COBBLESTONE_STAIRS, (byte)2, 20, 3f);
		
		x = 267;
		addBlock(x, 50, -47, Material.LADDER, (byte)4, 30, 3);
		addBlock(x, 48, -46);
		addBlock(x, 46, -47);
		addBlock(x, 44, -46);
		addBlock(x, 42, -47);
		addBlock(x, 41, -46);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockBreak(BlockBreakEvent event)
	{
		Block block = event.getBlock();
		if(explodableBlocks.containsKey(block.getLocation()))
		{
			ExplodableBlock eB = explodableBlocks.get(block.getLocation());
			eB.explode(true);
			event.setCancelled(true);
		}
	}
	
	public void placeBlocks()
	{
		for(Location loc : explodableBlocks.keySet())
		{
			ExplodableBlock expBlock = explodableBlocks.get(loc);
			expBlock.place();
		}
	}
	
	public void checkBlocks(long millis)
	{
		for(Location loc : explodableBlocks.keySet())
		{
			ExplodableBlock expBlock = explodableBlocks.get(loc);
			if(expBlock.getExplodeTime() != -1)
				if(millis > expBlock.getExplodeTime() + expBlock.getSecondsToRegen() * 1000L)
					expBlock.place();
		}
	}
	
	public void addBlock(int x, int y, int z, Material type, byte data, int secondsToRegen, float distance)
	{
		Location loc = new Location(mainWorld, x, y, z);
		tempMaterial = type;
		tempData = data;
		tempSeconds = secondsToRegen;
		tempDistance = distance;
		explodableBlocks.put(loc, new ExplodableBlock(loc, type, data, secondsToRegen, distance));
	}
	
	public void addBlock(int x, int y, int z)
	{
		Location loc = new Location(mainWorld, x, y, z);
		explodableBlocks.put(loc, new ExplodableBlock(loc, tempMaterial, tempData, tempSeconds, tempDistance));
	}
}
