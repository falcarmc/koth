package net.falcarmc.kotah.managers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.PlayerEntity;
import net.falcarmc.kotah.User;
import net.falcarmc.kotah.UtilClass;
import net.falcarmc.kotah.entities.ZombieParticle;
import net.falcarmc.kotah.managers.CrownManager.CrownType;
import net.falcarmc.kotah.util.ExplodableBlock;
import net.falcarmc.kotah.util.ItemType;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.sk89q.worldguard.bukkit.event.block.PlaceBlockEvent;

public class GameManager extends MainManager
{
	private Map<Long, List<ZombieParticle>> particles = new HashMap<>();
	public long hiddenMillis = -1;
	private UUID kingUUID = null;
	private long kingClaimedTime = -1;
	private Location shopLocation;
	public int[][] holeLocations = { { 249, -55 }, { 250, -57 }, { 251, -57 }, { 249, -56 }, { 250, -56 }, { 251, -56 }, { 252, -56 }, { 250, -55 }, { 251, -55 }, { 250, -54 }, { 251, -54 } };
	public int[][][] wallLocations = {};

	// private Map<PluginItem, ItemStack> defaultItems = new HashMap<>();

	public GameManager()
	{
		super();
	}

	public void onEnable()
	{
		shopLocation = new Location(mainWorld, 229.5, 24, 40.5, 0, 0);
		_("kingUUID is " + kingUUID);
		Collection<Player> players = getActivePlayers();
		if (players.size() > 0)
		{
			int randomId = random().nextInt(players.size());
			int id = 0;
			for (Player player : players)
			{
				if (id++ == randomId)
					setKing(player, "The KoTH plugin was reloaded (blame the devs!)");
			}
		}
		_("kingUUID now is " + kingUUID);
	}

	public Location getHoleLocation()
	{
		return new Location(mainWorld, holeLocations[0][0], 40, holeLocations[0][1]);
	}

	public void activateHoleButton()
	{
		if (hiddenMillis == -1)
			openHole();
		else if (System.currentTimeMillis() > hiddenMillis - (5 * 1000l))
			closeHole();
		else
			jammedHole();
	}

	public void openHole()
	{
		if (hiddenMillis == -1)
		{
			getHoleLocation().getWorld().playSound(getHoleLocation(), Sound.PISTON_RETRACT, 1f, 1f);

			setHole(Material.AIR, 0);

			setLadder(Material.LADDER, 0, true);
		}
		hiddenMillis = System.currentTimeMillis() + 10 * 1000l;
	}

	public void closeHole()
	{
		setLadder(Material.AIR, 0, false);
		setHole(Material.LOG, 13);
		mainWorld.playSound(getHoleLocation(), Sound.PISTON_EXTEND, 1f, 1f);
		hiddenMillis = -1;
	}

	public void jammedHole()
	{
		mainWorld.playSound(getHoleLocation(), Sound.CLICK, 1f, 1.7f + random().nextFloat() * 0.3f);
	}

	private void setHole(Material type, int data)
	{
		for (int[] locs : holeLocations)
		{
			Block block = mainWorld.getBlockAt(locs[0], 40, locs[1]);
			block.setType(type);
			block.setData((byte) data);
		}
	}

	private void setLadder(final Material type, final int data, boolean animation)
	{
		for (int y = 41, ticks = 0; y >= 30; y--, ticks += 2)
		{
			final int finalY = y;
			if (animation)
			{
				new BukkitRunnable()
				{
					@Override
					public void run()
					{
						setLoc(finalY, type, data);
					}
				}.runTaskLater(Main.instance, ticks);
			} else
			{
				setLoc(finalY, type, data);
			}
		}
	}

	private void setLoc(int y, Material type, int data)
	{
		Block block = mainWorld.getBlockAt(250, y, -54);
		block.setType(type);
		block.setData((byte) data);
	}

	public void setKingUUID(UUID uuid)
	{
		kingUUID = uuid;
	}

	public UUID getKingUUID()
	{
		return kingUUID;
	}

	@Override
	public boolean isKing(Player player)
	{
		return kingUUID != null && player != null && player.getUniqueId().equals(kingUUID);
	}

	public void setKingGracefully(Player player, Location crownLocation)
	{
		_("kingUUID was " + kingUUID);
		kingUUID = player.getUniqueId();
		_("Now it is " + kingUUID);
		kingClaimedTime = System.currentTimeMillis();
		HashSet<Player> activePlayers = (HashSet<Player>) getActivePlayers();
		activePlayers.add(player);
		for (Player aPlayer : activePlayers)
		{
			// _("Spawned player " + aPlayer.getName());
			equipPlayer(aPlayer, isKing(player), true);
			// getMain().getSpawnManager().spawn(aPlayer, false);
		}

		User uPlayer = getUser(player);
		CrownType crownType = uPlayer.getCrownType();
		getMain().getCrownManager().setCrownType(crownType);
		getMain().getCrownManager().createCrown(crownLocation, false);
	}

	public void setKing(Player player, String reason)
	{
		_("Set king: " + (player != null ? player.getName() : "null"));
		kingUUID = player != null ? player.getUniqueId() : null;
		if (kingUUID != null)
		{
			_("kingUUID is " + kingUUID);
			kingClaimedTime = System.currentTimeMillis();
			User uPlayer = getUser(player);
			CrownType crownType = uPlayer.getCrownType();
			getMain().getCrownManager().setCrownType(crownType);
			Main.getInstance().getCrownManager().createCrown(player, false);
			Set<Player> activePlayers = (HashSet<Player>) getActivePlayers();
			activePlayers.add(player);

			_("Active players are: " + activePlayers);
			for (Player eachPlayer : activePlayers)
			{
				_("Spawned player " + eachPlayer.getName());
				getMain().getSpawnManager().spawn(eachPlayer);
			}

			if (reason != null)
				for (Player eachPlayer : Bukkit.getOnlinePlayers())
					sendTitle(eachPlayer, player.getName() + " is now the King!", reason);
		} else
		{
			_("No one is the king");
			Main.getInstance().getCrownManager().clearCrownSegments();
			for (Player aPlayer : Bukkit.getOnlinePlayers())
			{
				_("Given " + aPlayer.getName() + " peasant armour.");
				equipPlayer(aPlayer, false, true);
				// aPlayer.getInventory().setHelmet(armor[3]);
				// aPlayer.getInventory().setChestplate(armor[2]);
				// aPlayer.getInventory().setLeggings(armor[1]);
				// aPlayer.getInventory().setBoots(armor[0]);
			}
			kingClaimedTime = -1;
		}
	}

	public void equipPlayer(Player player, boolean king, boolean rocket)
	{
		boolean rocketLauncher = false;
		User uPlayer = getUser(player);

		if (rocket)
			rocketLauncher = getMain().getRocketPackageManager().hasRocketLauncher(player);
		_(player.getName() + " has rocket launcher: " + rocketLauncher);
		player.getInventory().clear();
		if (player.getItemOnCursor() != null)
			player.setItemOnCursor(null);
		ItemStack[] armor = (king ? getMain().getSpawnManager().getKingArmour() : getMain().getSpawnManager().getPeasantArmour()).clone();
		player.getInventory().setArmorContents(armor);

		ItemStack sword = uPlayer.getItems().contains(ItemType.SWORD) ? getItem(ItemType.SWORD) : getItem(ItemType.REGULAR_SWORD);

		player.getInventory().addItem(sword);

		// give the original sword to them in case they want a fair fight
		if (uPlayer.getItems().contains(ItemType.SWORD))
			player.getInventory().setItem(8, getItem(ItemType.REGULAR_SWORD));

		if (king)
			player.getInventory().addItem(getItem(ItemType.ZOMBIE_GUN).clone());

		if (uPlayer.getItems().contains(ItemType.SPEED))
		{
			uPlayer.setLastSpeedUseTime(-1);
			uPlayer.getInventory().addItem(getItem(ItemType.SPEED).clone());
		}

		if (rocketLauncher)
		{
			_("Giving rocket launcher...");
			player.getInventory().addItem(getItem(ItemType.ROCKET_LAUNCHER).clone());
		}
	}

	public Player getKing()
	{
		return Bukkit.getPlayer(kingUUID);
	}

	public double getKingDuration()
	{
		return Math.ceil((System.currentTimeMillis() - kingClaimedTime) / 1000d);
	}

	public void sendTitle(Player player, String title, String subtitle)
	{
		sendTitle(player, ChatColor.RED, title, ChatColor.YELLOW, subtitle, 20 * 7);
	}

	public void sendTitle(Player player, ChatColor titleColor, String title, ChatColor subtitleColor, String subtitle, int duration)
	{
		if (title.length() > 0)
			player.sendMessage(getSpaces((int) Math.ceil(((subtitle.length() - title.length()) * 1.5f) / 2f)) + titleColor.toString() + ChatColor.BOLD + title);
		if (subtitle.length() > 0)
			player.sendMessage(subtitleColor + subtitle);

		IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + ChatColor.stripColor(title) + "\",color:" + titleColor.name().toLowerCase() + "}");
		IChatBaseComponent subTitle = ChatSerializer.a("{\"text\": \"" + ChatColor.stripColor(subtitle) + "\",color:" + subtitleColor.name().toLowerCase() + "}");

		PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(EnumTitleAction.TITLE, chatTitle);
		PacketPlayOutTitle subtitlePacket = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, subTitle);
		PacketPlayOutTitle lengthPacket = new PacketPlayOutTitle(5, duration, 5);

		((CraftPlayer) player).getHandle().playerConnection.sendPacket(titlePacket);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(subtitlePacket);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(lengthPacket);
	}

	private String getSpaces(int spaces)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < spaces; i++)
		{
			builder.append(" ");
		}
		return builder.toString();
	}

	public void witherSkullHit(PlayerEntity playerEntity, Location location)
	{
		User uDamager = playerEntity.getUser();
		Player damager = uDamager.getPlayer();
		Set<Player> deadPlayers = new HashSet<>();
		Map<Player, Double> damages = new HashMap<>();
		for (Player aPlayer : getActivePlayers())
		{
			User uPlayer = getUser(aPlayer);
			if (!uPlayer.isInvincible())
			{
				double damage = getRocketDamage(aPlayer, location);
				if (damage > 0)
				{
					boolean dead = aPlayer.getHealth() <= damage;
					if (dead)
						deadPlayers.add(aPlayer);
					else
						damages.put(aPlayer, damage);
				}
			}
		}

		// check for the king
		for (Player aPlayer : deadPlayers)
		{
			User uPlayer = getUser(aPlayer);
			kingCheck: if (isKing(aPlayer))
			{
				boolean suicide = false;
				if (uDamager.getUniqueId().equals(aPlayer.getUniqueId()))
				{
					damager = uPlayer.getLastDamager();
					if (damager == null)
						break kingCheck;
					suicide = true;
				}

				if (damager.isOnline() && !deadPlayers.contains(damager))
				{
					setKing(damager, suicide ? aPlayer.getName() + " exploded themselves while fighting " + damager.getName() : aPlayer.getName() + " was exploded by " + damager.getName()
							+ "'s rocket.");

					uPlayer.addDeath();
					uDamager.addKill(aPlayer, true);
					return;
				}
			}
		}

		// the king was not killed. proceed to deal damage to everyone.
		for (Player aPlayer : deadPlayers)
		{
			User uPlayer = getUser(aPlayer);
			getMain().getSpawnManager().spawn(aPlayer);
			uPlayer.addDeath();
			uDamager.addKill(aPlayer, false);
			sendTitle(aPlayer, "You died!", (aPlayer.getUniqueId() == playerEntity.getUser().getUniqueId() ? "Good job. 'Nuff said." : "You were exploded by " + playerEntity.getUser().getName()
					+ "'s rocket!"));
		}

		if (deadPlayers.contains(playerEntity.getUser().getPlayer()))
			deadPlayers.remove(playerEntity.getUser().getPlayer());

		if (deadPlayers.size() > 0)
		{
			String message = "You exploded " + UtilClass.listOfPlayers(deadPlayers) + "!";
			uDamager.playKillSound();
			sendTitle(playerEntity.getUser().getPlayer(), ChatColor.WHITE, "", ChatColor.YELLOW, message, 30);
			playerEntity.getUser().sendMessage(ChatColor.YELLOW + message);
		}

		Set<Player> tempKeySet = new HashSet<Player>(damages.keySet());
		tempKeySet.remove(damager);
		if (tempKeySet.size() > 0)
		{
			uDamager.attackedPlayer();
		}

		for (Player aPlayer : damages.keySet())
		{
			double damage = damages.get(aPlayer);
			double percentage = damage / 25;
			aPlayer.damage(damage);
			User uPlayer = getUser(aPlayer);
			uPlayer.setLastDamager(damager);
			Vector velocity = aPlayer.getLocation().toVector().subtract(location.toVector()).normalize().multiply(percentage * 3);
			aPlayer.setVelocity(velocity);
		}

		// explodable blocks //
		long millis = System.currentTimeMillis();
		for (Location loc : getMain().getBlockManager().explodableBlocks.keySet())
		{
			ExplodableBlock bl = getMain().getBlockManager().explodableBlocks.get(loc);
			Location middleOfLoc = loc.clone().add(0.5, 0.5, 0.5);
			if (middleOfLoc.distance(playerEntity.getEntity().getLocation()) < bl.getDistance())
				bl.explode(true, millis);
		}
		// ----------------- //

		// entity.getEntity().remove();
	}

	private double getRocketDamage(Player player, Location location)
	{
		double distance = player.getLocation().distance(location);
		if (distance < 5)
		{
			double damage = (25d * ((5 - distance) / 5d));
			_(damage + " is the damage.");
			return damage;
		}
		return 0;
		// return player.getWorld().equals(location.getWorld()) && player.getLocation().distance(location) <= 3;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPhysics(BlockPhysicsEvent event)
	{
		Block block = event.getBlock();
		if (block.getType() == Material.LADDER || block.getType() == Material.LONG_GRASS || block.getType() == Material.RED_ROSE || block.getType() == Material.YELLOW_FLOWER
				|| block.getType() == Material.STATIONARY_WATER || block.getType() == Material.WATER)
		{
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		Player player = event.getPlayer();
		User uPlayer = getUser(player);
		Player damager = uPlayer.getLastDamager();
		if (damager != null)
		{
			uPlayer.addDeath();
			getUser(damager).addKill(player);
		}
		_(player.getName() + " left the game.");
		if (kingUUID != null && event.getPlayer().getUniqueId().equals(kingUUID))
		{
			_(" - and they were the king. Allocating new player.");
			List<Player> activePlayers = new ArrayList<Player>(getActivePlayers());
			activePlayers.remove(event.getPlayer());
			if (activePlayers.size() > 0)
			{
				_("At least one active player");
				int index = random().nextInt(activePlayers.size());
				Player newKing = activePlayers.get(index);
				setKing(newKing, player.getName() + " left the game.");
			} else
			{
				_("No active players.");
				kingUUID = null;
				Main.getInstance().getCrownManager().clearCrownSegments();
			}
		}
	}

	@EventHandler
	public void onPaintingBreak(HangingBreakByEntityEvent event)
	{
		Hanging entity = event.getEntity();
		Entity damager = event.getRemover();
		if (damager.getType() == EntityType.PLAYER)
		{
			Player player = (Player) damager;
			if (entity.getType() == EntityType.PAINTING)
			{
				if (entity.getLocation().getBlockX() == 250 && entity.getLocation().getBlockY() == 33 && entity.getLocation().getBlockZ() == -51)
				{
					if (player.getGameMode() == GameMode.SURVIVAL)
					{
						activateHoleButton();
					}
				}
			}

			if (player.getGameMode() != GameMode.CREATIVE)
				event.setCancelled(true);
		} else
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event)
	{
		if (event.getEntityType() == EntityType.PLAYER)
		{
			Player player = (Player) event.getEntity();
			if (event.getCause() == DamageCause.FALL)
			{
				double damage = event.getDamage();
				double finalDamage = event.getDamage() * 0.5;
				List<Player> players = new ArrayList<Player>();
				for (Player aPlayer : player.getWorld().getPlayers())
				{
					User uPlayer = getUser(aPlayer);
					if (aPlayer != player && aPlayer.getLocation().distance(player.getLocation()) < 1 && !uPlayer.isInvincible())
					{
						players.add(aPlayer);
					}
				}

				if (players.size() > 0)
				{
					event.setDamage(finalDamage * 0.25);
					List<Player> playersToKill = new ArrayList<>();
					double damageEach = finalDamage / players.size();
					for (Player aPlayer : players)
					{
						User uDamager = getUser(player);
						User uPlayer = getUser(aPlayer);
						if (damageEach >= aPlayer.getHealth())
						{
							double newDamage = finalDamage * 1.5;
							if (event.getDamage() < player.getHealth() && isKing(aPlayer))
							{
								event.setDamage(0);
								player.setHealth(20);
								setKing(player, player.getName() + " landed on " + aPlayer.getName() + " and broke his neck!");
								uDamager.addKill(aPlayer);
								uPlayer.addDeath();
								return;
							}
							playersToKill.add(aPlayer);
						} else
						{
							aPlayer.damage(damageEach);
							addFallEffect(aPlayer, damage);
						}
					}

					String message = "You landed on " + UtilClass.listOfPlayers(players) + " and softened your fall!";
					sendTitle(player, ChatColor.WHITE, "", ChatColor.YELLOW, message, 30);
					player.sendMessage(ChatColor.YELLOW + message);

					for (Player aPlayer : playersToKill)
					{
						getMain().getSpawnManager().spawn(aPlayer);
						sendTitle(aPlayer, "You died!", player.getName() + " landed on you and broke your neck!");
					}

				} else
				{
					if (damage > 6)
					{
						addFallEffect(player, damage);
					}
					event.setDamage(finalDamage);
				}
			}

			if (event.getCause() != DamageCause.ENTITY_ATTACK)
			{
				double finalDamage = event.getFinalDamage();
				if (finalDamage >= player.getHealth())
				{
					User uPlayer = getUser(player);
					Player damager = uPlayer.getLastDamager();
					uPlayer.addDeath();
					if (damager != null)
					{
						User uDamager = getUser(damager);
						uDamager.addKill(player);
					}
					if (isKing(player))
					{
						if (damager != null)
						{
							String reason = player.getName() + " " + getDeathString(event.getCause()) + " " + damager.getName();
							setKing(damager, reason);
						}
					}
					event.setCancelled(true);
					player.damage(0);
					getMain().getSpawnManager().spawn(player);
				}
			}
		}
	}

	public Collection<Player> getActivePlayers()
	{
		Set<Player> players = new HashSet<>();
		for (Player player : mainWorld.getPlayers())
			if (!Main.getInstance().isInRegion(Main.getInstance().getRegionSet(player.getLocation()), "shop"))
				players.add(player);
		return players;
	}

	public void addFallEffect(Player player, double fallDamage)
	{
		int seconds = (int) (10 * ((fallDamage - 4) / 2f));
		player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, seconds, 2, true, false));
		player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, seconds, 0, true, false));
	}

	public String getDeathString(DamageCause cause)
	{
		switch (cause)
		{
		case DROWNING:
			return "was forced to drown by";
		case FIRE:
			return "was thrown into fire by";
		case FIRE_TICK:
			return "burned while fighting";
		case FALL:
			return "was thrown from a height by";
		case LAVA:
			return "was thrown into lava by";
		case SUFFOCATION:
			return "suffocated while fighting";
		case THORNS:
			return "was killed trying to attack";
		case VOID:
			return "was thrown into the void by";
		default:
			return "was slain by";
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		Player player = event.getPlayer();
		Action action = event.getAction();
		ItemStack item = player.getItemInHand();
		if (action != Action.PHYSICAL)
		{
			if (item.isSimilar(getItem(ItemType.SPEED)))
			{
				getMain().getUserManager().getUser(player).useSpeedTool();
				event.setCancelled(true);
				return;
			}
		}

		if (action == Action.LEFT_CLICK_BLOCK)
		{
			Block block = event.getClickedBlock();
			if (block.getLocation().getBlockX() == 261 && block.getLocation().getBlockY() == 44 && block.getLocation().getBlockZ() == -47)
			{
				activateHoleButton();
				event.setCancelled(true);
			}
		} else if (action == Action.RIGHT_CLICK_BLOCK)
		{
			Block block = event.getClickedBlock();
			if (player.getGameMode() != GameMode.CREATIVE && UtilClass.isContainer(block.getType()))
			{
				event.setCancelled(true);
			}
		}
		if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK))
		{
			if (item.isSimilar(getItem(ItemType.ROCKET_LAUNCHER)))
			{
				getMain().getUserManager().getUser(player).fireRocket();
			} else if (item.isSimilar(getItem(ItemType.ZOMBIE_GUN)))
			{
				getMain().getUserManager().getUser(player).useKingTool();
			}
		}
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event)
	{
		Player player = (Player) event.getEntity();
		if (player.getFoodLevel() < 20)
			player.setFoodLevel(20);
		event.setCancelled(true);
	}

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event)
	{
		event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		boolean realigned = false;

		// new block
		if (event.getTo().getBlockX() != event.getFrom().getBlockX() || event.getTo().getBlockY() != event.getFrom().getBlockY() || event.getTo().getBlockZ() != event.getFrom().getBlockZ())
		{
			long millis = System.currentTimeMillis();
			double x = event.getTo().getX(), y = event.getTo().getY(), z = event.getTo().getZ();
			int bX = event.getTo().getBlockX(), bY = event.getTo().getBlockY(), bZ = event.getTo().getBlockZ();
			boolean portal = bX >= 224 && bX <= 234 && bY >= 26 && bY <= 40 && bZ >= 44 && bZ <= 46;

			User uPlayer = getUser(player);
			if (uPlayer.isInvincible() && (uPlayer.getLastSpawn() == null || uPlayer.getLastSpawn().distance(event.getTo()) >= 3))
				uPlayer.setInvincible(false);

			if (isKing(player))
			{
				realigned = getMain().getCrownManager().checkSync(player, event.getFrom(), event.getTo());
			}

			if (!player.isFlying())
			{
				boolean goingIntoShop = getMain().isInRegion(getMain().getRegionSet(event.getTo()), "shop") && !getMain().isInRegion(getMain().getRegionSet(event.getFrom()), "shop");
				boolean leavingShop = getMain().isInRegion(getMain().getRegionSet(event.getFrom()), "shop") && !getMain().isInRegion(getMain().getRegionSet(event.getTo()), "shop");
				boolean inShop = getMain().isInRegion(getMain().getRegionSet(event.getTo()), "shop");
				// if player is in the waterfall, push them backwards. If
				// they're sneaking, push them up slightly (so they can't sneak
				// on the edge of the waterfall.
				if (!(bX == 175 && bY == 50 && bZ == -67)
						&& ((x >= 171.7 && x <= 178.3 && bY >= 22 && bY <= 49 && z >= -61.3 && z <= -59.7) || (bY == 50 && ((bX >= 172 && bX <= 177 && bZ >= -69 && bZ <= -61) || (bX >= 174
								&& bX <= 177 && bZ == -70)))))
				{
					double yVal = 0;
					if (player.isSneaking())
						yVal = 0.2;
					player.setVelocity(new Vector(player.getVelocity().getX(), player.getVelocity().getY() + yVal, 0.5));
				}
				// if there are more than 1 active players and the king tries to
				// get in the shop, throw them backwards. if they are the only
				// active player, allow them to pass through
				else if (isKing(player) && getActivePlayers().size() > 1 && portal)
				{
					player.setVelocity(new Vector(0, 0.1, -2));
					if (uPlayer.getLastShopMessage() < millis - 10 * 1000l)
					{
						uPlayer.setLastShopMessage(millis);
						player.sendMessage(ChatColor.YELLOW + "(Shop) " + ChatColor.RED + "You are not able to enter the shop as the king.");
						player.sendMessage(ChatColor.YELLOW + "(Shop) " + ChatColor.RED + "To give someone else the crown, use /peasant.");
						player.sendMessage(ChatColor.YELLOW + "(Shop) " + ChatColor.RED + "If there are no other active players in the game, you are able to enter.");
					}
				}
				// if the king gets into the shop, teleport him to outside the shop
				// spawn
				else if (isKing(player) && inShop)
				{
					Collection<Player> activePlayers = getActivePlayers();
					if ((activePlayers.size() > 0) && !(activePlayers.size() == 1 && goingIntoShop))
					{
						player.teleport(shopLocation);
					} else if (goingIntoShop)
					{
						setKing(null, null);
					}
				}
				// if no active players (outside shop) are king, and someone walks out of shop, give them the crown
				else if (kingUUID == null && leavingShop)
				{
					setKingGracefully(player, event.getTo());
					getMain().getCrownManager().updateCrownSegmentLocations(event.getTo());
				}
				// if someone has attacked the player recently, prevent them from going into the shop

				if (!uPlayer.outOfCombat())
					if (portal)
					{
						player.setVelocity(new Vector(0, 0.1, -2));
						if (uPlayer.getLastShopMessage() < millis - 10 * 1000l)
						{
							uPlayer.setLastShopMessage(millis);
							player.sendMessage(ChatColor.YELLOW + "(Shop) " + ChatColor.RED + "You must be out of combat for at least 20 seconds to enter.");
						}
					} else if (inShop)
						player.teleport(shopLocation);

			}

			// if the player is in reach of the rocket package and wasn't in the
			// region before
			getMain().getRocketPackageManager().checkPlayerPickupRocket(player, event.getFrom(), event.getTo());

		}

		// if the crown hasn't been synced in this move event
		if (!realigned && isKing(player))
		{
			// move the crown segments in the same direction the player walked
			// (to save from calculating the positions with math every time
			Vector difference = event.getTo().clone().subtract(event.getFrom()).toVector();
			getMain().getCrownManager().moveCrown(difference);
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		Player player = event.getPlayer();
		if (player.getGameMode() != GameMode.CREATIVE)
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event)
	{
		Player player = event.getPlayer();
		if (player.getGameMode() != GameMode.CREATIVE)
			event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		if (kingUUID == null)
		{
			setKing(player, null);
		} else
		{
			getMain().getSpawnManager().spawn(player);
		}
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kdebug");
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event)
	{
		Player player = event.getPlayer();
		if (event.getCause() == TeleportCause.NETHER_PORTAL || event.getCause() == TeleportCause.END_PORTAL)
		{
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onEntityTeleport(EntityTeleportEvent event)
	{
		if (event.getTo().getWorld() != event.getFrom().getWorld())
		{
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event)
	{
		Player player = event.getPlayer();
		ItemStack item = event.getItemDrop().getItemStack();
		if (player.getGameMode() != GameMode.CREATIVE)
			event.setCancelled(true);
	}

	// @EventHandler
	// public void onProjectileHit(EntityDamageByEntityEvent event)
	// {
	// Entity e1 = event.getEntity();
	// Entity e2 = event.getDamager();
	// if (e1 instanceof ArmorStand)
	// {
	// ArmorStand stand = (ArmorStand) e1;
	// if (pieces.contains(stand))
	// {
	// event.setCancelled(true);
	// }
	// }
	// }

	public void showWaterfallTunnel()
	{

	}

	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event)
	{
		event.blockList().clear();
	}

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event)
	{
		event.blockList().clear();
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event)
	{
		Entity e1 = event.getEntity(), e2 = event.getDamager();

		User uPlayer = null;
		if (e1.getType() == EntityType.PLAYER)
		{
			uPlayer = getUser((Player) e1);
			if (uPlayer.isInvincible())
			{
				event.setCancelled(true);
				return;
			}
		}

		User uDamager = null;
		if (e2.getType() == EntityType.PLAYER)
		{
			uDamager = getUser((Player) e2);
			if (uDamager.isInvincible())
			{
				event.setCancelled(true);
				return;
			}
		}

		if(event.isCancelled())
			return;
		
		if (e1.getType() == EntityType.PLAYER && e2.getType() == EntityType.PLAYER)
		{
			Player player = (Player) e1;
			Player damager = (Player) e2;

			// User uPlayer = getMain().getUserManager().getUser(player);
			uPlayer.setLastDamager(damager);
			uDamager.attackedPlayer();

			double finalDamage = event.getFinalDamage();
			if (finalDamage >= player.getHealth())
			{
				uPlayer.addDeath();
				uDamager.addKill(player);
				if (isKing(player))
				{
					setKing(damager, player.getName() + " was slain by " + damager.getName());
				}
				event.setCancelled(true);
				player.damage(0);

				getMain().getSpawnManager().spawn(player);
			}
		} else if (e1.getType() == EntityType.PLAYER && e2.getType() == EntityType.ZOMBIE)
		{
			Player player = (Player) e1;
			Zombie zombie = (Zombie) e2;

			for (PlayerEntity ent : getMain().getEntityManager().createdEntities)
			{
				if (ent.getEntity().getEntityId() == zombie.getEntityId())
				{
					Player damager = ent.getUser().getPlayer();
					uDamager = getUser(damager);
					double damage = (Math.random() * 4) + 12;
					event.setDamage(damage);
					damager.getWorld().playSound(damager.getLocation(), Sound.BURP, 1f, 1f);
					if (event.getFinalDamage() >= player.getHealth())
					{
						event.setCancelled(true);
						player.damage(0);
						sendTitle(player, "You died!", (player.getUniqueId() == damager.getUniqueId() ? "Maybe.. try not to walk into your own zombies?" : "One of " + damager.getName()
								+ "'s zombies killed you!"));
						// getMain().getUserManager().getUser(damager).addTempKill();
						uPlayer.addDeath();
						uDamager.addKill(player);
						getMain().getSpawnManager().spawn(player);
					}
					break;
				}
			}
		} else if (e1.getType() == EntityType.ZOMBIE && e2.getType() == EntityType.PLAYER)
		{
			Zombie zombie = (Zombie) e1;
			Player player = (Player) e2;
			if (kingUUID != null && player.equals(getMain().getEntityManager().getEntityOwner(zombie)) && kingUUID == player.getUniqueId())
			{
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onZombieTarget(EntityTargetLivingEntityEvent event)
	{
		Entity e = event.getEntity();
		Entity target = event.getTarget();
		if (target == null || target.getType() != EntityType.PLAYER)
			return;
		Player pTarget = (Player) target;
		User uTarget = getUser(pTarget);
		if (kingUUID != null && e.getType() == EntityType.ZOMBIE && (isKing(pTarget) || uTarget.isInvincible()))
		{
			Player closestPlayer = getClosestPlayer(e.getLocation());
			if (closestPlayer != null)
				event.setTarget(closestPlayer);
			else
				event.setCancelled(true);
		}
	}

	private Player getClosestPlayer(Location loc)
	{
		double distanceSquared = 625;
		Player player = null;
		for (Player aPlayer : loc.getWorld().getPlayers())
		{
			if (isKing(aPlayer) || getUser(aPlayer).isInvincible())
				continue;

			double tempDist = aPlayer.getLocation().distanceSquared(loc);
			if (tempDist < distanceSquared)
			{
				distanceSquared = tempDist;
				player = aPlayer;
			}
		}
		return player;
	}

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event)
	{
		event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event)
	{
		event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent event)
	{
		event.setCancelled(true);
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		event.getDrops().clear();
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event)
	{
		Player player = event.getPlayer();
		event.setRespawnLocation(getMain().getSpawnManager().getSpawnLocation(player));
		getMain().getSpawnManager().spawn(player);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		SlotType slot = event.getSlotType();
		if (slot == SlotType.CRAFTING)
		{
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onInventoryDrag(InventoryDragEvent event)
	{
		Set<Integer> slots = event.getRawSlots();
		Inventory inv = event.getInventory();
		if (inv.getType() == InventoryType.CRAFTING)
			for (int i = 1; i <= 4; i++)
				if (slots.contains(i))
				{
					event.setCancelled(true);
					break;
				}
	}

	public void checkHole()
	{
		if (hiddenMillis != -1 && System.currentTimeMillis() > hiddenMillis)
		{
			closeHole();
		}
	}
}
