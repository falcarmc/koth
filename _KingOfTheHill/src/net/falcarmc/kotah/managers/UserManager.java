package net.falcarmc.kotah.managers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.User;
import net.falcarmc.kotah.util.ItemType;
import net.falcarmc.kotah.util.SimpleScoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class UserManager extends MainManager
{
	private Map<UUID, User> users = new HashMap<>();

	public UserManager()
	{
		super();
		reloadOnlineUsers();
	}

	public Map<UUID, User> getUsers()
	{
		return users;
	}

	public void onEnable()
	{
		reloadOnlineUsers();
	}

	public void onDisable()
	{
		for (User user : users.values())
			getMain().getConfigManager().saveUserToConfig(user);
	}

	public void displayScoreboardToAllPlayers()
	{
		for (User user : users.values())
		{
			if (user != null && user.isOnline())
				displayScoreboard(user);
			else
				_(user.getName() + " is not online?!");
		}
	}

	private void displayScoreboard(User user)
	{
		SimpleScoreboard sS = user.getSimpleScoreboard();
		sS.clear();
		sS.blankLine();

		Player king = Main.getInstance().getGameManager().getKing();
		String name;
		if (king == null)
			name = "No one";
		else
			name = king.getName();

		sS.add(ChatColor.RED + "King: " + ChatColor.GOLD + name);
		if (king != null)
		{
			sS.add(ChatColor.RED + "- Health: " + ChatColor.GOLD + String.format("%.1f", (Math.ceil(king.getHealth()) / 2f)));
			int secondsLeft = getUser(king).getSecondsLeftOnKingTool();
			String secondsString;
			if (secondsLeft == -1)
				secondsString = "Reloaded";
			else
				secondsString = secondsLeft + "s";
			sS.add(ChatColor.RED + "- Zombies: " + ChatColor.GOLD + secondsString);
		}
		sS.blankLine();

		sS.add(ChatColor.YELLOW + "Kills: " + ChatColor.GOLD + user.getKills(false));
		sS.add(ChatColor.YELLOW + "Deaths: " + ChatColor.GOLD + user.getDeaths(false));
		sS.add(ChatColor.YELLOW + "Points: " + ChatColor.GOLD + user.getTokens());
		sS.blankLine();
		int rocketSeconds = getMain().getRocketPackageManager().packageSeconds - (int) Math.ceil((System.currentTimeMillis() - getMain().getRocketPackageManager().lastRocketTime) / 1000f);
		int minutes = (int) (rocketSeconds / 60f);
		int seconds = rocketSeconds % 60;
		String rocketString = "";
		if (minutes > 0)
			rocketString += minutes + "m";
		if (seconds > 0)
			rocketString += String.format((minutes > 0 ? "%02d" : "%d"), seconds) + "s";
		if (rocketString.length() == 0)
			rocketString = "Relocating";

		sS.add(ChatColor.GRAY + "Package: " + ChatColor.GOLD + rocketString);
		boolean hasRocket = getMain().getRocketPackageManager().hasRocketLauncher(user);
		if (hasRocket)
			sS.add(ChatColor.GRAY + "- Ammo: " + ChatColor.GOLD.toString() + user.getRocketAmmo());
		
		boolean inCombat = !user.outOfCombat();
		if(inCombat)
		{
			sS.blankLine();
			int mostRecentAttack = (int)Math.ceil((15000l - (System.currentTimeMillis() - Math.max(user.getLastAttack(), user.getLastPlayerAttack())))/1000f);
			sS.add(ChatColor.BLUE + "PvpTimer: " + ChatColor.GOLD + mostRecentAttack + "s");
		}

		sS.draw();
		sS.send(user.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		loadUser(player);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		Player player = event.getPlayer();
		unloadUser(player);
	}

	public void checkSpeedPotions()
	{
		for (User user : users.values())
		{
			if (user.getLastSpeedUseTime() != -1 && !user.isInSpeedTimeout())
			{
				user.setLastSpeedUseTime(-1);
				ItemStack speed = getItem(ItemType.SPEED);
				ItemStack emptySpeed = getItem(ItemType.EMPTY_SPEED);
				if (user.getItemOnCursor() != null && user.getItemOnCursor().isSimilar(emptySpeed))
				{
					user.setItemOnCursor(speed);
				} else
				{
					user.getInventory().remove(emptySpeed);
					user.getInventory().addItem(speed);
				}
			}
		}
	}

	public void reloadOnlineUsers()
	{
		users.clear();
		for (Player player : Bukkit.getOnlinePlayers())
		{
			loadUser(player);
		}
	}

	public void saveUsers()
	{
		for (User user : users.values())
		{
			if (user != null)
				getMain().getConfigManager().saveUserToConfig(user);
		}
	}

	public void loadUser(Player player)
	{
		if (getUser(player.getUniqueId()) == null)
		{
			User user = new User(player);
			getMain().getConfigManager().loadUserFromConfig(user);
			users.put(player.getUniqueId(), user);
		}
	}

	public void unloadUser(Player player)
	{
		User user = getUser(player.getUniqueId());
		if (user != null)
		{
			getMain().getConfigManager().saveUserToConfig(user);
			users.remove(user.getUniqueId());
			user.getInventory().clear();
		}
	}

	public User getUser(Player player)
	{
		return getUser(player.getUniqueId());
	}

	public User getUser(UUID uuid)
	{
		return users.get(uuid);
	}

	// public User getUser(String name)
	// {
	// for (User user : users)
	// {
	// if (user.getName().equals(name))
	// {
	// return user;
	// }
	// }
	// return null;
	// }
}
