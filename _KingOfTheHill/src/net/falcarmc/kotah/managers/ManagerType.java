package net.falcarmc.kotah.managers;

public enum ManagerType
{
	CROWN, CONFIG, GAME, ITEM, SHOP, SPAWN, ROCKET_PACKAGE, USER, ENTITY, COMMAND, BLOCK, SCOREBOARD;
}
