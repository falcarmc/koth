package net.falcarmc.kotah.managers;

import java.util.Random;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.User;
import net.falcarmc.kotah.util.ItemType;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public abstract class MainManager implements Listener
{
	Main main;
	World mainWorld;
	
	public MainManager()
	{
		main = Main.getInstance();
		mainWorld = main.mainWorld;
	}
	
	public void onEnable()
	{
		// override in subclass
	}
	
	public void onDisable()
	{
		// override in subclass
	}
	
	public Main getMain()
	{
		return Main.getInstance();
	}
	
	public ItemStack getItem(ItemType type)
	{
		return Main.getInstance().getItemManager().getItem(type);
	}
	
	public boolean isKing(Player player)
	{
		return Main.getInstance().getGameManager().isKing(player);
	}
	
	public Random random()
	{
		return main.random;
	}
	
	public User getUser(Player player)
	{
		return getMain().getUserManager().getUser(player);
	}
	
	public void _(String message)
	{
//		Bukkit.broadcastMessage(this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().indexOf("Manager")) + "> " + message);
	}
}
