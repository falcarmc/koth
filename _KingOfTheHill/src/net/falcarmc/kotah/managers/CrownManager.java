package net.falcarmc.kotah.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.falcarmc.kotah.util.CrownLocation;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class CrownManager extends MainManager
{
	public enum CrownGroup
	{
		USER(0, ChatColor.WHITE), ALPHA(1, ChatColor.GOLD), BETA(11, ChatColor.BLUE), GAMMA(5, ChatColor.GREEN), DELTA(10, ChatColor.DARK_PURPLE);

		private byte data;
		private ChatColor color;

		CrownGroup(int data, ChatColor color)
		{
			this.data = (byte) data;
			this.color = color;
		}

		public byte getData()
		{
			return data;
		}

		public ChatColor getColor()
		{
			return color;
		}
		
		private String permission()
		{
			return "falcar." + name().toLowerCase();
		}
		
		public boolean hasPermission(Player player)
		{
			return (this == CrownGroup.USER ? true : player.hasPermission(permission()));
		}
	}

	public enum CrownType
	{
		YELLOW, GOLDEN, DIRTY, SPONGE, ORANGE, QUARTZ, CRAFTED_WOODEN, BLUE, PUMPKIN, REDSTONE,
		CRAFTED_STONEBRICK, GREEN, MELON, DIAMOND, CRAFTED_QUARTZ, PURPLE, OBSIDIAN, EMERALD,
		CRAFTED_NETHER, RAINBOW, KNOWLEDGE, SEA_BLOCKS, END_STONE, HAY, FROZEN, BEDROCK, SNOW,
		WALL, CAMO, TNT, SLIME, OAK;

		String title;

		CrownType()
		{
			StringBuilder builder = new StringBuilder();
			for (String part : name().toLowerCase().split("_"))
				builder.append((builder.length() > 0 ? " " : "") + part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase());
			this.title = builder.toString();
		}
	}

	private List<ArmorStand> pieces = new ArrayList<>();
	private CrownType type = CrownType.GOLDEN;
	private Map<CrownGroup, CrownType[]> crowns = new HashMap<>();
	private int[] killsForCrown = { 0, 10, 50, 100, 200, 350, 500, 1000 };
	private int[] rainbowColour = { 14, 1, 4, 5, 13, 3, 9, 11, 10, 6 };
	private int[][] randomSeeds = {new int[20], new int[20]};

	public int blocksWalkedTillCrownUpdate = 0;

	public CrownManager()
	{
		super();
		popularCrownMap();
	}
	
	public void popularCrownMap()
	{
		addCrown(CrownGroup.USER, CrownType.YELLOW, 0);
		addCrown(CrownGroup.USER, CrownType.DIRTY, 1);
		addCrown(CrownGroup.USER, CrownType.SPONGE, 2);
		addCrown(CrownGroup.USER, CrownType.OAK, 3);
		addCrown(CrownGroup.USER, CrownType.HAY, 4);
		addCrown(CrownGroup.USER, CrownType.GOLDEN, 7);

		addCrown(CrownGroup.ALPHA, CrownType.ORANGE, 0);
		addCrown(CrownGroup.ALPHA, CrownType.MELON, 1);
		addCrown(CrownGroup.ALPHA, CrownType.END_STONE, 2);
		addCrown(CrownGroup.ALPHA, CrownType.QUARTZ, 4);
		addCrown(CrownGroup.ALPHA, CrownType.CRAFTED_WOODEN, 6);
		addCrown(CrownGroup.ALPHA, CrownType.PUMPKIN, 7);

		addCrown(CrownGroup.BETA, CrownType.BLUE, 0);
		addCrown(CrownGroup.BETA, CrownType.KNOWLEDGE, 1);
		addCrown(CrownGroup.BETA, CrownType.SNOW, 3);
		addCrown(CrownGroup.BETA, CrownType.REDSTONE, 4);
		addCrown(CrownGroup.BETA, CrownType.CRAFTED_STONEBRICK, 6);
		addCrown(CrownGroup.BETA, CrownType.TNT, 7);

		addCrown(CrownGroup.GAMMA, CrownType.GREEN, 0);
		addCrown(CrownGroup.GAMMA, CrownType.CAMO, 1);
		addCrown(CrownGroup.GAMMA, CrownType.FROZEN, 3);
		addCrown(CrownGroup.GAMMA, CrownType.DIAMOND, 4);
		addCrown(CrownGroup.GAMMA, CrownType.SLIME, 4);
		addCrown(CrownGroup.GAMMA, CrownType.CRAFTED_QUARTZ, 6);
		addCrown(CrownGroup.GAMMA, CrownType.WALL, 7);

		addCrown(CrownGroup.DELTA, CrownType.PURPLE, 0);
		addCrown(CrownGroup.DELTA, CrownType.BEDROCK, 2);
		addCrown(CrownGroup.DELTA, CrownType.SEA_BLOCKS, 3);
		addCrown(CrownGroup.DELTA, CrownType.EMERALD, 4);
		addCrown(CrownGroup.DELTA, CrownType.OBSIDIAN, 5);
		addCrown(CrownGroup.DELTA, CrownType.CRAFTED_NETHER, 6);
		addCrown(CrownGroup.DELTA, CrownType.RAINBOW, 7);

		for(int y=0; y<randomSeeds.length; y++)
			for (int i = 0; i < randomSeeds[y].length; i++)
				randomSeeds[y][i] = random().nextInt(1000);
	}

	public int getKillsForTier(int tier)
	{
		return killsForCrown[tier];
	}

	public ItemStack getBlock(CrownType type, int rotation, int height)
	{
		int randomSeed = randomSeeds[height][rotation];
		switch (type)
		{
		case YELLOW:
			return new ItemStack(Material.WOOL, 1, (byte) 4);
		case GOLDEN:
			return new ItemStack(Material.GOLD_BLOCK);
		case DIRTY:
			return new ItemStack(Material.DIRT, 1, (byte) (randomSeed % 2));
		case SPONGE:
			return new ItemStack(Material.SPONGE, 1, (byte) (randomSeed % 2));
		case ORANGE:
			return new ItemStack(Material.WOOL, 1, (byte) 1);
		case OAK:
			return new ItemStack(Material.LOG, 1);
		case QUARTZ:
			return new ItemStack(Material.QUARTZ_BLOCK);
		case CRAFTED_WOODEN:
			return new ItemStack(height == 0 ? Material.WOOD : Material.WOOD_STAIRS);
		case BLUE:
			return new ItemStack(Material.WOOL, 1, (byte) 11);
		case PUMPKIN:
			return new ItemStack(rotation % 2 == 0 ? Material.PUMPKIN : Material.JACK_O_LANTERN, 1, (byte) 3);
		case REDSTONE:
			return new ItemStack(Material.REDSTONE_BLOCK);
		case CRAFTED_STONEBRICK:
			return new ItemStack(height == 0 ? Material.SMOOTH_BRICK : Material.SMOOTH_STAIRS);
		case GREEN:
			return new ItemStack(Material.WOOL, 1, (byte) 5);
		case MELON:
			return new ItemStack(Material.MELON_BLOCK);
		case DIAMOND:
			return new ItemStack(Material.DIAMOND_BLOCK);
		case CRAFTED_QUARTZ:
			return new ItemStack(height == 0 ? Material.QUARTZ_BLOCK : Material.QUARTZ_STAIRS);
		case PURPLE:
			return new ItemStack(Material.WOOL, 1, (byte) 10);
		case OBSIDIAN:
			return new ItemStack(Material.OBSIDIAN);
		case EMERALD:
			return new ItemStack(Material.EMERALD_BLOCK);
		case CRAFTED_NETHER:
			return new ItemStack(height == 0 ? Material.NETHER_BRICK : Material.NETHER_BRICK_STAIRS);
		case RAINBOW:
			// {
			// return new ItemStack(Material.LEATHER_HELMET);
			return new ItemStack(Material.STAINED_CLAY, 1, (byte) rainbowColour[rotation % rainbowColour.length]);
		case BEDROCK:
			return new ItemStack(Material.BEDROCK);
		case END_STONE:
			return new ItemStack(Material.ENDER_STONE);
		case FROZEN:
			return new ItemStack((randomSeed % 2) == 0 ? Material.ICE : Material.PACKED_ICE);
		case HAY:
			return new ItemStack(Material.HAY_BLOCK);
		case KNOWLEDGE:
			return new ItemStack(Material.BOOKSHELF);
		case SEA_BLOCKS:
		{
			int typeId = (randomSeed % 4);
			return new ItemStack(typeId == 3 ? Material.SEA_LANTERN : Material.PRISMARINE, 1, (byte) ((typeId == 3 ? 0 : typeId)));
		}
		case SNOW:
			return new ItemStack(Material.SNOW_BLOCK);
		case WALL:
			return new ItemStack(Material.COBBLE_WALL, 1, (byte) (randomSeed % 2));
			// }
		case CAMO:
			return new ItemStack(Material.LEAVES, 1, (byte) (randomSeed % 4));
//		case COBWEBS:
//			return new ItemStack(Material.WEB);
		case TNT:
			return new ItemStack(Material.TNT);
		case SLIME:
			return new ItemStack(Material.SLIME_BLOCK);
		}

		return new ItemStack(Material.STONE);
	}

	public CrownType[] getCrowns(CrownGroup cG)
	{
		return crowns.get(cG);
	}

	public void addCrown(CrownGroup cG, CrownType cT, int index)
	{
		if (!crowns.containsKey(cG))
			crowns.put(cG, new CrownType[8]);
		CrownType[] crownList = crowns.get(cG);
		crownList[index] = cT;
	}

	public void setCrownType(CrownType type)
	{
		this.type = type;
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event)
	{
		Player player = event.getPlayer();
		if (getMain().getGameManager().isKing(player))
			updateCrownSegmentLocations(event.getTo());
	}

	public void moveCrown(Vector difference)
	{
		for (ArmorStand stand : pieces)
			stand.teleport(stand.getLocation().clone().add(difference));
	}

	public void onDisable()
	{
		clearCrownSegments();
	}

	public void createCrown(Player player, boolean small)
	{
		if (!small && pieces.size() > 0)
			clearCrownSegments();
		createCrown(player.getLocation(), small);
	}

	public void createCrown(Location centerLocation, boolean small)
	{
		List<CrownLocation> locations = calculateCrownSegmentLocations(getCenterLocation(getEyeLocation(centerLocation)), small);
		for (CrownLocation location : locations)
			summonCrownSegment(location, small);
	}

	public void clearCrownSegments()
	{
		for (ArmorStand stand : pieces)
			stand.remove();
		pieces.clear();
	}

	public Location getCenterLocation(Location location)
	{
		return location.clone().add(0, 1d, 0);
	}

	public void updateCrownSegmentLocations()
	{
		Player player = getMain().getGameManager().getKing();
		if (player != null)
		{
			updateCrownSegmentLocations(player.getLocation());
		}
	}

	public void updateCrownSegmentLocations(Location location)
	{
		List<CrownLocation> locations = calculateCrownSegmentLocations(getCenterLocation(getEyeLocation(location)), false);
		for (int i = 0; i < locations.size(); i++)
		{
			ArmorStand stand = pieces.get(i);
			stand.teleport(locations.get(i).getLoc());
		}
	}

	public Location getEyeLocation(Location location)
	{
		return location.clone().add(0, 1.62D, 0);
	}

	public List<CrownLocation> calculateCrownSegmentLocations(Location centerLocation, boolean small)
	{
		List<CrownLocation> locations = new ArrayList<>();
		int rotations = small ? 18 : 20;
		int i = 0;
		for (float theta = 0; i < rotations; theta += (Math.PI * 2) / rotations, i++) // theta < Math.PI * 2
		{
			int repetitions = 1;
			if (i % 2 == 0)
				repetitions = 2;
			for (int rep = 0; rep < repetitions; rep++)
			{
				Location newLocation = new Location(centerLocation.getWorld(), centerLocation.getX(), centerLocation.getY(), centerLocation.getZ(), (float) Math.toDegrees((theta))
						+ (small ? 19.25f : 0) - (type == CrownType.PUMPKIN ? 90 : 180), 0f);
				double radius = small ? 0.475 : 1.15f;
				newLocation.add(radius * Math.cos(theta), small ? (rep * 0.125) : (1.75 - 1.6d + rep * 0.22), radius * Math.sin(theta));
				locations.add(new CrownLocation(newLocation, i, rep));
			}
		}
		return locations;
	}

	public void summonCrownSegment(CrownLocation location, boolean small)
	{
		Location loc = location.getLoc();
		// if (true)
		// loc.setYaw(loc.getYaw() + 90);
		ArmorStand stand = (ArmorStand) location.getLoc().getWorld().spawnEntity(location.getLoc(), EntityType.ARMOR_STAND);
		ItemStack block = getBlock(type, location.getIndex(), location.getHeight());
		stand.setRemoveWhenFarAway(false);
		if (small)
		{
			stand.setArms(true);
			stand.setRightArmPose(new EulerAngle(Math.toRadians(14), Math.toRadians(220), Math.toRadians(340))); // 14f,220f,340
			stand.getEquipment().setItemInHand(block);
		} else
			stand.setHelmet(block);

		stand.setSmall(true);
		stand.setBasePlate(false);
		stand.setVisible(false);
		stand.setGravity(false);
		stand.setMarker(true);
		if (!small)
			pieces.add(stand);
	}

	public boolean checkSync(Player player, Location getFrom, Location getTo)
	{
		// recalculate the crown segment locations every 10 blocks the
		// player walks.
		blocksWalkedTillCrownUpdate += 1;
		if (blocksWalkedTillCrownUpdate >= 10)
		{
			blocksWalkedTillCrownUpdate = blocksWalkedTillCrownUpdate % 10;
			updateCrownSegmentLocations(getTo);
			return true;
		}
		return false;
	}
}
