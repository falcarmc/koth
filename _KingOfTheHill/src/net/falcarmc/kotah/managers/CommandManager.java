package net.falcarmc.kotah.managers;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.commands.CreateCommand;
import net.falcarmc.kotah.commands.ForfeitCommand;
import net.falcarmc.kotah.commands.KDebugCommand;
import net.falcarmc.kotah.commands.ShopCommand;


public class CommandManager extends MainManager
{
	public CommandManager()
	{
		super();
		Main.getInstance().getCommand("kdebug").setExecutor(new KDebugCommand());
		Main.getInstance().getCommand("create").setExecutor(new CreateCommand());
		Main.getInstance().getCommand("forfeit").setExecutor(new ForfeitCommand());
		Main.getInstance().getCommand("shop").setExecutor(new ShopCommand());
	}
}
