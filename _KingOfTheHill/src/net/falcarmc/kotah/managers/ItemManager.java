package net.falcarmc.kotah.managers;

import java.util.HashMap;
import java.util.Map;

import net.falcarmc.kotah.util.EnchantmentGlow;
import net.falcarmc.kotah.util.ItemType;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffectType;

public class ItemManager extends MainManager
{
	// public ItemStack rocketLauncher, kingTool;
	private Map<ItemType, ItemStack> items = new HashMap<>();
	public Enchantment glowEnchantment = new EnchantmentGlow(70);

	public ItemManager()
	{
		super();

		// rocket launcher tool
		{
			ItemStack item = new ItemStack(Material.IRON_BARDING, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.WHITE + "Rocket Launcher");
			item.setItemMeta(meta);
			items.put(ItemType.ROCKET_LAUNCHER, item);
		}

		// kings zombie tool
		{
			ItemStack item = new ItemStack(Material.ROTTEN_FLESH);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.WHITE + "Zombie Smokescreen");
			item.setItemMeta(meta);
			items.put(ItemType.ZOMBIE_GUN, item);
		}

		{
			items.put(ItemType.REGULAR_SWORD, unbreak(new ItemStack(Material.STONE_SWORD)));
		}

		{
			items.put(ItemType.SWORD, unbreak(new ItemStack(Material.IRON_SWORD)));
		}

		{
			ItemStack potion = new ItemStack(Material.POTION);
			PotionMeta potionMeta = (PotionMeta) potion.getItemMeta();
			potionMeta.setMainEffect(PotionEffectType.SPEED);
			potionMeta.setDisplayName(ChatColor.WHITE + "Speed");
			potion.setItemMeta(potionMeta);
			items.put(ItemType.SPEED, potion);
		}
		
		{
			ItemStack emptyPotion = new ItemStack(Material.GLASS_BOTTLE);
			ItemMeta meta = emptyPotion.getItemMeta();
			meta.setDisplayName(ChatColor.WHITE + "Empty Speed");
			emptyPotion.setItemMeta(meta);
			items.put(ItemType.EMPTY_SPEED, emptyPotion);
		}
	}
	
	private ItemStack unbreak(ItemStack item)
	{
		ItemMeta meta = item.getItemMeta();
		meta.spigot().setUnbreakable(true);
		item.setItemMeta(meta);
		return item;
	}

	public ItemStack getItem(ItemType type)
	{
		return items.get(type).clone();
	}
}
