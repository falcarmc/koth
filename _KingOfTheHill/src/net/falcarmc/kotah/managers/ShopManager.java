package net.falcarmc.kotah.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.User;
import net.falcarmc.kotah.managers.CrownManager.CrownGroup;
import net.falcarmc.kotah.managers.CrownManager.CrownType;
import net.falcarmc.kotah.util.EnchantmentGlow;
import net.falcarmc.kotah.util.ItemType;
import net.falcarmc.kotah.util.PluginItemInfo;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.EulerAngle;

public class ShopManager extends MainManager
{
	public ArmorStand rotatingSword = null, rotatingSwordHolo = null, crownHolo = null, compassHolo = null, speedHolo = null;
	public int rotatingIndex = 0, rotations = 30, nearCheck = 0;
	public boolean lastNearCheck = false;
	public Location rotatingSwordLocation, crownHoloLocation, compassHoloLocation, speedHoloLocation;
	public Location insideShopLocation;
	public String crownInventoryName = "Customise Crown";
	private String statsLorePrefix = ChatColor.GRAY + "- ";
	private ItemStack nullItem = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 8);
	{
		ItemMeta meta = nullItem.getItemMeta();
		meta.setDisplayName(ChatColor.RED + "N/A");
		nullItem.setItemMeta(meta);
	}
	private Map<ItemType, PluginItemInfo> prices = new HashMap<>();

	public ShopManager(World mainWorld)
	{
		super();
		rotatingSwordLocation = new Location(mainWorld, 229.5, 26.0, 52.5);
		Location holoLocation = rotatingSwordLocation.clone().add(0, 2.25, 0);
		crownHoloLocation = new Location(mainWorld, 233.5, 28.25, 53.5);
		for (Entity entity : rotatingSwordLocation.getChunk().getEntities())
		{
			if (entity.getType() == EntityType.ARMOR_STAND)
			{
				ArmorStand stand = (ArmorStand) entity;
				if (stand.getItemInHand() != null && stand.getItemInHand().getType() == Material.IRON_SWORD)
				{
					if (entity.getLocation().distance(rotatingSwordLocation) < 1.0)
					{
						// the armorstand is the rotating sword
						rotatingSword = stand;
					}
				} else if (entity.getLocation().distance(holoLocation) < 1.5)
				{
					rotatingSwordHolo = stand;
				}
			}
		}
		if (rotatingSword == null)
			rotatingSword = (ArmorStand) rotatingSwordLocation.getWorld().spawnEntity(rotatingSwordLocation, EntityType.ARMOR_STAND);
		else
			rotatingSword.teleport(rotatingSwordLocation);

		rotatingSword.setItemInHand(new ItemStack(Material.IRON_SWORD));
		rotatingSword.setArms(true);
		rotatingSword.setRightArmPose(new EulerAngle(Math.toRadians(270), Math.toRadians(315), Math.toRadians(20)));
		rotatingSword.setGravity(false);
		rotatingSword.setVisible(false);
		rotatingSword.setMarker(true);

		if (rotatingSwordHolo == null)
			rotatingSwordHolo = (ArmorStand) rotatingSwordLocation.getWorld().spawnEntity(holoLocation, EntityType.ARMOR_STAND);
		else
			rotatingSwordHolo.teleport(holoLocation);

		setHologram(rotatingSwordHolo, "Iron Sword (400 points)");

		for (Entity entity : crownHoloLocation.getChunk().getEntities())
		{
			if (entity.getType() == EntityType.ARMOR_STAND)
			{
				ArmorStand stand = (ArmorStand) entity;
				if ((stand.getItemInHand() == null || stand.getItemInHand().getType() == Material.AIR) && !stand.isSmall())
				{
					if (stand.getLocation().distance(crownHoloLocation) < 1.0)
					{
						crownHolo = stand;
					}
				}
			}
		}
		if (crownHolo == null)
			crownHolo = (ArmorStand) crownHoloLocation.getWorld().spawnEntity(crownHoloLocation, EntityType.ARMOR_STAND);
		else
			crownHolo.teleport(crownHoloLocation);

		setHologram(crownHolo, "Customise Crown");

		// TODO: FIX THIS MESS. GOSH..

		speedHoloLocation = new Location(mainWorld, 225.5, 28.25, 53.5);
		for (Entity entity : speedHoloLocation.getChunk().getEntities())
		{
			if (entity.getType() == EntityType.ARMOR_STAND)
			{
				ArmorStand stand = (ArmorStand) entity;
				if ((stand.getItemInHand() == null || stand.getItemInHand().getType() == Material.AIR) && !stand.isSmall())
				{
					if (stand.getLocation().distance(speedHoloLocation) < 1.0)
					{
						speedHolo = stand;
					}
				}
			}
		}
		if (speedHolo == null)
			speedHolo = (ArmorStand) speedHoloLocation.getWorld().spawnEntity(speedHoloLocation, EntityType.ARMOR_STAND);
		else
			speedHolo.teleport(speedHoloLocation);

		setHologram(speedHolo, "Speed Tool (250 points)");

		// TODO: THIS IS CRAZY

//		compassHoloLocation = new Location(mainWorld, 221.5, 28.25, 53.5);
//		for (Entity entity : compassHoloLocation.getChunk().getEntities())
//		{
//			if (entity.getType() == EntityType.ARMOR_STAND)
//			{
//				ArmorStand stand = (ArmorStand) entity;
//				if ((stand.getItemInHand() == null || stand.getItemInHand().getType() == Material.AIR) && !stand.isSmall())
//				{
//					if (stand.getLocation().distance(compassHoloLocation) < 1.0)
//					{
//						compassHolo = stand;
//					}
//				}
//			}
//		}
//		if (compassHolo == null)
//			compassHolo = (ArmorStand) compassHoloLocation.getWorld().spawnEntity(compassHoloLocation, EntityType.ARMOR_STAND);
//		else
//			compassHolo.teleport(compassHoloLocation);
//
//		setHologram(compassHolo, "Rocket Locator (250 points)");

		insideShopLocation = new Location(mainWorld, 229.5, 26, 49);
		
		prices.put(ItemType.SWORD, new PluginItemInfo(229, 27, 52, 400));
		prices.put(ItemType.SPEED, new PluginItemInfo(225, 27, 53, 250));

	}

	public void setHologram(ArmorStand stand, String name)
	{
		stand.setCustomName(name);
		stand.setCustomNameVisible(true);
		stand.setGravity(false);
		stand.setVisible(false);
		stand.setMarker(true);
	}

	public boolean playerNear()
	{
		// only loop over the players to check their distance every second;
		// otherwise return the cached value.
		if ((nearCheck = ((nearCheck + 1) % (20 * 1))) == 0)
		{
			for (Player player : rotatingSwordLocation.getWorld().getPlayers())
			{
				if (player.getLocation().distanceSquared(rotatingSwordLocation) < 225)
				{
					lastNearCheck = true;
					return lastNearCheck;
				}
			}
			lastNearCheck = false;
			return lastNearCheck;
		}
		return lastNearCheck;
	}

	public void rotateStand()
	{
		if (playerNear())
		{
			rotatingIndex = (rotatingIndex + 1) % rotations;
			rotatingSword.teleport(getRotationLocation(rotatingSwordLocation, rotatingIndex));
		}
	}

	public void showCrownInventory(Player player)
	{
		Inventory inv = Bukkit.createInventory(player, 9 * 6, crownInventoryName);
		User user = getMain().getUserManager().getUser(player);
		ItemStack skull = user.getHead().clone();
		{
			ItemMeta meta = skull.getItemMeta();
			meta.setLore(infoLore(player));
			meta.setDisplayName(ChatColor.GRAY + "Your Stats:");
			skull.setItemMeta(meta);
		}
		inv.setItem(4, skull);
		for (int y = 1; y <= CrownGroup.values().length; y++)
		{
			CrownGroup cG = CrownGroup.values()[y - 1];
			boolean perm = cG.hasPermission(player);
			ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, cG.getData());
			ItemMeta meta = glass.getItemMeta();
			meta.setDisplayName(cG.getColor() + cG.name().substring(0, 1) + cG.name().toLowerCase().substring(1) + " Blocks");
			glass.setItemMeta(meta);
			inv.setItem(getInvSlot(0, y), glass);
			CrownType[] crowns = getMain().getCrownManager().getCrowns(cG);
			for (int x = 1; x <= crowns.length; x++)
			{
				CrownType cT = crowns[x - 1];
				boolean selected = cT == user.getCrownType();
				if (cT != null)
				{
					ItemStack block = getMain().getCrownManager().getBlock(cT, 0, 1);
					List<String> lore = new ArrayList<>();
					int killsNeeded = getMain().getCrownManager().getKillsForTier(x - 1);
					int playerKills = user.getKills(true);
					boolean unlocked = playerKills >= killsNeeded;
					lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "Tier " + x);
					if (perm)
					{
						if (killsNeeded > 0)
						{
							if (!unlocked)
							{
								float percent = ((float) playerKills) / killsNeeded;
								int number = (int) (Math.ceil(percent * 100));
								lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "You are " + number + "% there");
							} else
							{
								lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "You unlocked this at " + killsNeeded + " kills.");
							}
						} else
							lore.add(ChatColor.GRAY + ChatColor.ITALIC.toString() + "FREE");
					} else
					{
						lore.add(ChatColor.GRAY + "You need the " + cG.getColor() + ChatColor.BOLD.toString() + cG.name() + ChatColor.GRAY + " rank to");
						lore.add(ChatColor.GRAY + "     access this.");
					}
					ItemMeta blockMeta = block.getItemMeta();
					blockMeta.setDisplayName((selected ? ChatColor.YELLOW + ">> " : "") + (unlocked && perm ? ChatColor.GREEN : ChatColor.RED) + cT.title
							+ (!unlocked ? ChatColor.YELLOW + " (" + killsNeeded + " kills)" : "") + (selected ? ChatColor.YELLOW + " <<" : ""));
					blockMeta.setLore(lore);
					if (selected)
						blockMeta.addEnchant(getMain().getItemManager().glowEnchantment, 0, false);
					block.setItemMeta(blockMeta);
					inv.setItem(getInvSlot(x, y), block);
				} else
				{
					inv.setItem(getInvSlot(x, y), nullItem.clone());
				}
			}
		}
		player.openInventory(inv);
	}

	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event)
	{
		Inventory inv = event.getInventory();
		Player player = (Player) event.getWhoClicked();
		if (inv.getTitle().equals(crownInventoryName))
		{
			int[] coords = getInvSlot(event.getRawSlot());
			int x = coords[0];
			int y = coords[1];
			if (x >= 1 && y >= 1)
			{
				CrownGroup group = CrownGroup.values()[y - 1];
				User uPlayer = getUser(player);
				int killsNeeded = getMain().getCrownManager().getKillsForTier(x - 1);
				int playerKills = uPlayer.getKills(true);
				boolean unlocked = playerKills >= killsNeeded;
				if (unlocked && group.hasPermission(player))
				{
					CrownType type = getMain().getCrownManager().getCrowns(CrownGroup.values()[y - 1])[x - 1];
					if (type != null && uPlayer.getCrownType() != type)
					{
						uPlayer.setCrownType(type);
						player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1f, 2f);
						showCrownInventory(player);
					}
				} else
				{
					player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1f, 0.5f);
				}
			}
			event.setCancelled(true);
		}
	}

	private int getInvSlot(int x, int y)
	{
		return y * 9 + x;
	}

	private int[] getInvSlot(int slot)
	{
		int x = slot % 9;
		int y = (int) (slot / 9f);
		return new int[] { x, y };
	}

	private List<String> infoLore(Player player)
	{
		List<String> info = new ArrayList<>();
		User user = Main.getInstance().getUserManager().getUser(player);
		info.add(statsLorePrefix + "Kills: " + user.getPermKills() + " (+" + user.getKills(false) + ")");
		info.add(statsLorePrefix + "Deaths: " + user.getPermDeaths() + " (+" + user.getDeaths(false) + ")");
		return info;
	}

	public Location getRotationLocation(Location centerLocation, int rotation)
	{
		Location location = centerLocation.clone();
		double theta = (Math.PI * 2) * ((float) rotation / rotations);
		location.setX(location.getX() + 0.45 * Math.cos(theta));
		location.setZ(location.getZ() + 0.45 * Math.sin(theta));
		location.setYaw((float) (((Math.toDegrees(theta) - 90.0) % 360.0) - 180.0));
		location.setPitch(0);
		return location;
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		// new block
		if (event.getTo().getBlockX() != event.getFrom().getBlockX() || event.getTo().getBlockY() != event.getFrom().getBlockY() || event.getTo().getBlockZ() != event.getFrom().getBlockZ())
		{
			double x = event.getTo().getX(), y = event.getTo().getY(), z = event.getTo().getZ();
			int bX = event.getTo().getBlockX(), bY = event.getTo().getBlockY(), bZ = event.getTo().getBlockZ();
			if (bX == 233 && bY == 27 && bZ == 53)
			{
				// stepped onto crown pressureplate
				showCrownInventory(player);
			}

			for (ItemType item : prices.keySet())
			{
				PluginItemInfo info = prices.get(item);
				_("Checking plate for: " + item);
				if (bX == info.getX() && bY == info.getY() && bZ == info.getZ())
				{
					User user = getUser(player);
					if (!user.getItems().contains(item))
					{
						int price = info.getPrice();
						_("Has stepped on " + item.toString() + " pressureplate for " + price + ".");
						if (price <= user.getTokens())
						{
							_("Bought successfully for " + price);
							user.removeTokens(price);
							user.addItem(item);
							getMain().getGameManager().equipPlayer(player, false, true);
							user.playSound(user.getLocation(), Sound.CHICKEN_EGG_POP, 1f, 1.5f);
						} else
						{
							user.playSound(user.getLocation(), Sound.CHICKEN_EGG_POP, 1f, 0.5f);
						}
					}
					break;
				}
			}
		}
	}
}
