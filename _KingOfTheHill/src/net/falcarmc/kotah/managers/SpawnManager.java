package net.falcarmc.kotah.managers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.User;

import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class SpawnManager extends MainManager
{
	public List<Location> playerSpawns = new ArrayList<>();
	public Location kingSpawn;
	private ItemStack[] kingArmour = new ItemStack[4], peasantArmour = new ItemStack[4];

	public SpawnManager(World mainWorld)
	{
		super();
		kingSpawn = new Location(mainWorld, 260.5f, 42.5f, -46.5, 90, 0);

		playerSpawns.add(new Location(mainWorld, 189.5, 22.0, -54.5, -90, -15));
		playerSpawns.add(new Location(mainWorld, 189.5, 22.0, -44.5, -90, -15));
		playerSpawns.add(new Location(mainWorld, 189.5, 22.0, -34.5, -90, -15));
		playerSpawns.add(new Location(mainWorld, 193.5, 22.0, -24.5, -130, -15));
		playerSpawns.add(new Location(mainWorld, 231.5, 22.0, 13.5, -140, -15));
		playerSpawns.add(new Location(mainWorld, 241.5, 22.0, 17.5, -180, -15));
		playerSpawns.add(new Location(mainWorld, 251.5, 22.0, 17.5, -180, -15));
		playerSpawns.add(new Location(mainWorld, 261.5, 22.0, 17.5, -180, -15));

		for (int i = 0; i < 2; i++)
		{
			ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
			ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
			ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
			ItemStack[] armour = { boots, leggings, chestplate, helmet };
			for (ItemStack item : armour)
			{
				ItemMeta meta = item.getItemMeta();
				meta.spigot().setUnbreakable(true);
				item.setItemMeta(meta);
			}
			if (i == 0)
			{
				{
					LeatherArmorMeta meta = (LeatherArmorMeta) helmet.getItemMeta();
					meta.setColor(Color.fromRGB(255, 226, 81));
					helmet.setItemMeta(meta);
				}
				{
					LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
					meta.setColor(Color.fromRGB(255, 71, 58));
					chestplate.setItemMeta(meta);
					leggings.setItemMeta(meta);
				}
				{
					LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
					meta.setColor(Color.fromRGB(38, 128, 255));
					boots.setItemMeta(meta);
				}
				setKingArmour(armour);
			} else
			{
				setPeasantArmour(armour);
			}
		}
	}

	public void spawn(Player player)
	{
		_("Spawned player " + player.getName());
		spawn(player, true);
	}

	public void spawn(final Player player, boolean teleport)
	{
		player.setHealth(20);
		player.setFireTicks(0);
		for (PotionEffect potionEffect : new HashSet<PotionEffect>(player.getActivePotionEffects()))
			player.removePotionEffect(potionEffect.getType());

		User uPlayer = getUser(player);
		if (teleport)
		{
			uPlayer.setLastDamager(null);
			_("Teleporting " + player.getName());
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 0, true, false));
			new BukkitRunnable()
			{
				@Override
				public void run()
				{
					player.setVelocity(new Vector(0, 0, 0));
				}
			}.runTaskLater(Main.instance, 0);

			Location spawnLocation = getSpawnLocation(player);
			player.teleport(spawnLocation);
			uPlayer.setLastSpawn(spawnLocation);
			uPlayer.setLastAttack(-1);
		}

		if (isKing(player))
		{
			_("Giving " + player.getName() + " the crown");
			getMain().getCrownManager().createCrown(player, false);
		}

		_("Giving armor...");
		getMain().getGameManager().equipPlayer(player, isKing(player), !teleport);

		if (teleport)
		{
			_("Setting item slot to 1");
			player.getInventory().setHeldItemSlot(0);
			if(!isKing(player))
			{
				uPlayer.setInvincible(true);
			}
		}
		
		player.setGameMode(GameMode.SURVIVAL);
	}

	public Location getSpawnLocation(Player player)
	{
		if (isKing(player))
		{
			return kingSpawn;
		} else
		{
			return playerSpawns.get(random().nextInt(playerSpawns.size()));
		}
	}

	public ItemStack[] getKingArmour()
	{
		return kingArmour;
	}

	public void setKingArmour(ItemStack[] kingArmour)
	{
		this.kingArmour = kingArmour;
	}

	public ItemStack[] getPeasantArmour()
	{
		return peasantArmour;
	}

	public void setPeasantArmour(ItemStack[] peasantArmour)
	{
		this.peasantArmour = peasantArmour;
	}
}
