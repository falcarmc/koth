package net.falcarmc.kotah.managers;

import org.bukkit.ChatColor;

public class MessageManager
{
	public static String LOGO = ChatColor.YELLOW.toString();

	public static String NO_PERM = LOGO + ChatColor.translateAlternateColorCodes('&', "You are lacking \'&f%s'&e to use this command");
	public static String PLAYER_ONLY = LOGO + ChatColor.translateAlternateColorCodes('&', "You must be the in-game to use this command");
	public static String USAGE = LOGO + ChatColor.translateAlternateColorCodes('&', "/%s %s &8-&e %s");
	public static String INCORRECT_BLOCK = LOGO + "You are not looking at a %s block.";
	public static String NOT_ENOUGH_MONEY = LOGO + "You do not have enough money.";
	public static String NOT_ENOUGH_MONEY_WITH_MONEY = LOGO + "You are lacking $%s to use this";

	public static String capitalise(String name)
	{
		StringBuilder builder = new StringBuilder();
		for (String part : name.split(" "))
			builder.append((builder.length() > 0 ? " " : "") + part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase());
		return builder.toString();
	}
}
