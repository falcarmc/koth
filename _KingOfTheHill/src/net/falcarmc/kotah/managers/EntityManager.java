package net.falcarmc.kotah.managers;

import java.util.ArrayList;
import java.util.List;

import net.falcarmc.kotah.PlayerEntity;
import net.falcarmc.kotah.PlayerEntity.PEntType;
import net.falcarmc.kotah.User;
import net.falcarmc.kotah.entities.EntityRocket;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.GenericAttributes;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class EntityManager extends MainManager
{
	public List<PlayerEntity> createdEntities = new ArrayList<>();

	public EntityManager()
	{
		super();
	}
	
	public void onDisable()
	{
		for(PlayerEntity entity : createdEntities)
			entity.remove();
	}

	public void addEntityToUser(User user, PEntType type, Entity entity, int ticksUntilPurged)
	{
		createdEntities.add(new PlayerEntity(entity, type, user, ticksUntilPurged));
	}

	public User getEntityOwner(Entity entity)
	{
		for (PlayerEntity playerEntity : createdEntities)
		{
			if (playerEntity.getEntity().getEntityId() == (entity.getEntityId()))
				return playerEntity.getUser();
		}
		return null;
	}

	public PlayerEntity getEntityPlayerEntity(Entity entity)
	{
		for (PlayerEntity playerEntity : createdEntities)
		{
			if (playerEntity.getEntity().getEntityId() == entity.getEntityId())
				return playerEntity;
		}
		return null;
	}

	public List<PlayerEntity> getAllUserEntities(User user)
	{
		List<PlayerEntity> entities = new ArrayList<>();
		for (PlayerEntity playerEntity : createdEntities)
		{
			if (playerEntity.getUser().getUniqueId().equals(user.getUniqueId()))
				entities.add(playerEntity);
		}
		return entities;
	}
	
	public boolean isEntityType(Entity e, PEntType type)
	{
		for(PlayerEntity pE : createdEntities)
		{
			if(pE.getType() == type && pE.getEntity().getEntityId() == e.getEntityId())
			{
				return true;
			}
		}
		return false;
	}

	public void clearOldEntities()
	{
		List<PlayerEntity> removedEntities = new ArrayList<>();
		for (PlayerEntity entity : createdEntities)
		{
			boolean dead = entity.getEntity() == null || entity.getEntity().isDead();
			if (dead || entity.getEntity().getTicksLived() > entity.getTicksUntilPurged())
			{
				if (!dead)
					entity.getEntity().remove();
				removedEntities.add(entity);
			}
		}
		createdEntities.removeAll(removedEntities);
	}

	public Zombie spawnZombie(Location location)
	{
		Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);
		zombie.setBaby(false);
		if (zombie.getVehicle() != null)
			zombie.getVehicle().remove();
		zombie.setVillager(false);
		zombie.getEquipment().setItemInHand(new ItemStack(Material.AIR));
		zombie.setCanPickupItems(false);
		((EntityLiving) ((CraftEntity) zombie).getHandle()).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.175);
		((EntityLiving) ((CraftEntity) zombie).getHandle()).getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(30);
		zombie.setHealth(1);
		return zombie;
	}

	public void fireRocket(Player player)
	{
		Location spawnLocation = player.getEyeLocation().clone().add(player.getEyeLocation().getDirection().normalize().multiply(0.8));
		Vector direction = player.getEyeLocation().getDirection().normalize().multiply(100);
		net.minecraft.server.v1_8_R3.World world = ((CraftWorld) getMain().mainWorld).getHandle();
		EntityRocket entity = new EntityRocket(world, ((EntityLiving) ((CraftPlayer) player).getHandle()), direction.getX(), direction.getY(), direction.getZ());
		entity.setCharged(true);

		entity.setPositionRotation(spawnLocation.getX(), spawnLocation.getY(), spawnLocation.getZ(), spawnLocation.getYaw(), spawnLocation.getPitch());
		world.addEntity(entity);

		Entity e = (Entity) entity.getBukkitEntity();

		User user = getMain().getUserManager().getUser(player);
		addEntityToUser(user, PEntType.ROCKET, e, 280);
		
		player.getWorld().playSound(player.getLocation(), Sound.WITHER_SHOOT, 1f, 0.75f);
	}

	@EventHandler
	public void onEntityLight(EntityCombustEvent event)
	{
		Entity entity = event.getEntity();
		for (PlayerEntity ent : createdEntities)
		{
			if (ent.getEntity().getEntityId() == entity.getEntityId())
			{
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityExplode(EntityExplodeEvent event)
	{
		Entity proj = event.getEntity();
		if (proj.getType() == EntityType.WITHER_SKULL)
		{
			PlayerEntity playerEntity = getEntityPlayerEntity(proj);
			if (playerEntity != null)
			{
				getMain().getGameManager().witherSkullHit(playerEntity, event.getLocation());
				// Bukkit.broadcastMessage(event.getYield() + "");
				event.setYield(0);
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{
		Entity e = event.getEntity();
		if(event.getCause() == DamageCause.FALL)
		{
			if(e.getType() == EntityType.ZOMBIE && isEntityType(e, PEntType.ZOMBIE))
			{
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDeath(EntityDeathEvent event)
	{
		if (event.getEntity().getType() == EntityType.ZOMBIE)
		{
			Zombie zombie = (Zombie) event.getEntity();
			for (PlayerEntity ent : createdEntities)
			{
				if (ent.getType() == PEntType.ZOMBIE && ent.getEntity().getEntityId() == zombie.getEntityId())
				{
					event.getDrops().clear();
				}
			}
		}
		event.setDroppedExp(0);
	}
}
