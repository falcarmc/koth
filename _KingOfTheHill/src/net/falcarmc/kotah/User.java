package net.falcarmc.kotah;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.falcarmc.kotah.PlayerEntity.PEntType;
import net.falcarmc.kotah.managers.CrownManager.CrownType;
import net.falcarmc.kotah.util.EnchantmentGlow;
import net.falcarmc.kotah.util.ItemType;
import net.falcarmc.kotah.util.SimpleScoreboard;

import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.Statistic;
import org.bukkit.WeatherType;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.InventoryView.Property;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.map.MapView;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;

public class User implements Player
{
	private List<ItemType> items = new ArrayList<>();
	private int perm_kills;
	private int perm_deaths;
	private int tokens;
	private int kills;
	private int deaths;
	private long last_logout;
	private Player player;
	// private List<ItemType> availableItems = new ArrayList<>();
	// private boolean recharged = true;
	private long lastRocketUseTime = -1;
	private long lastKingToolUseTime = -1;
	private long lastSpeedUseTime = -1;
	private ItemStack head;
	private Location lastSpawn = null;

	private int rocketLauncherAmmo = 10;

	private UUID lastPlayerToAttack = null;
	private long lastPlayerAttack = -1;

	private long lastAttack = -1;

	private long lastShopMessage = -1;
	// private long

	private SimpleScoreboard sS = new SimpleScoreboard(ChatColor.RED.toString() + ChatColor.BOLD + "King of The Hill");

	private boolean invincible = false;

	public CrownType crownType = CrownType.YELLOW;

	public User(Player player)
	{
		this.player = player;
		head = new ItemStack(Material.SIGN, 1);
		// SkullMeta meta = (SkullMeta)(head.getItemMeta());
		// meta.setOwner(player.getName());
		// head.setItemMeta(meta);
	}

	public SimpleScoreboard getSimpleScoreboard()
	{
		return sS;
	}

	public void fireRocket()
	{
		long millis = System.currentTimeMillis();
		long timeToBe = (long) (lastRocketUseTime + 5 * 1000);
		boolean king = getName().equals("KingDragonRider");

		if (rocketLauncherAmmo > 0 || king)
		{
			if (millis > timeToBe || king)
			{
				lastRocketUseTime = System.currentTimeMillis();

				if (!king)
					rocketLauncherAmmo--;
				Main.getInstance().getEntityManager().fireRocket(player);
			} else
			{
				int secondsLeft = (int) Math.ceil((timeToBe - millis) / 1000f);
				Main.getInstance().getGameManager().sendTitle(player, ChatColor.RESET, "", ChatColor.RED, "Reloading... (" + secondsLeft + "s)", 20);
			}
		} else
			Main.getInstance().getGameManager().sendTitle(player, ChatColor.RESET, "", ChatColor.RED, "Out of ammo", 20);
	}

	public void useKingTool()
	{
		if (isKing())
		{
			long millis = System.currentTimeMillis();
			double timeToBe = lastKingToolUseTime + 20 * 1000;
			if (millis >= timeToBe)
			{
				lastKingToolUseTime = millis;
				Block block = player.getTargetBlock((HashSet<Byte>) null, 3).getRelative(0, 1, 0);

				for (int i = 0; i < 15; i++)
				{
					float theta = (float) (Math.random() * Math.PI * 2);
					float distance = (float) Math.random() * 1f;
					Location location = block.getLocation().clone();
					location = location.add(new Vector(Math.cos(theta) * distance, 0, Math.sin(theta) * distance));
					int originalY = location.getBlockY();
					for (int y = originalY + 2; y >= originalY - 2; y--)
					{
						if (location.getBlock().getType() == Material.AIR)
							location = location.add(0, -1, 0);
						else
						{
							location = location.add(0, 1, 0);
							break;
						}
					}
					Main.getInstance().getEntityManager().addEntityToUser(this, PEntType.ZOMBIE, Main.getInstance().getEntityManager().spawnZombie(location), 20 * 8);
				}
			} else
			{
				int secondsLeft = (int) Math.ceil((timeToBe - millis) / 1000f);
				Main.getInstance().getGameManager().sendTitle(player, ChatColor.RESET, "", ChatColor.GOLD, "Reloading... (" + secondsLeft + "s)", 20);
			}
		}
	}

	public void useSpeedTool()
	{
		if (lastSpeedUseTime == -1)
		{
			long millis = System.currentTimeMillis();
			setLastSpeedUseTime(millis);
			player.getInventory().setItem(player.getInventory().getHeldItemSlot(), Main.getInstance().getItemManager().getItem(ItemType.EMPTY_SPEED));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 8 * 20, 2, true, false));
			player.getWorld().playSound(player.getLocation(), Sound.DRINK, 1f, 1f);
		}
	}

	public boolean isInSpeedTimeout()
	{
		long millis = System.currentTimeMillis();
		return getLastSpeedUseTime() > millis - 30000l;
	}

	public long getLastSpeedUseTime()
	{
		return lastSpeedUseTime;
	}

	public void setLastSpeedUseTime(long lastSpeedUseTime)
	{
		this.lastSpeedUseTime = lastSpeedUseTime;
	}

	public long getLastPlayerAttack()
	{
		return lastPlayerAttack;
	}

	public int getRocketAmmo()
	{
		return rocketLauncherAmmo;
	}

	public void setRocketLauncherAmmo(int rocketLauncherAmmo)
	{
		this.rocketLauncherAmmo = rocketLauncherAmmo;
	}

	public void setItems(List<ItemType> items)
	{
		this.items = items;
	}

	public List<ItemType> getItems()
	{
		return items;
	}

	public void addItem(ItemType item)
	{
		if (items == null)
			items = new ArrayList<ItemType>();
		items.add(item);
	}

	public boolean isInvincible()
	{
		return !isKing() && invincible;
	}

	public void setInvincible(boolean isInvincible)
	{
		applyEnchArmourEffect(isInvincible);
		this.invincible = isInvincible;
	}

	public void applyEnchArmourEffect(boolean enabled)
	{
		ItemStack[] armorContents = getInventory().getArmorContents();
		for (int i = 0; i < 4; i++)
		{
			ItemStack item = armorContents[i];
			if (item == null || item.getType() == Material.AIR)
				continue;
			ItemMeta meta = item.getItemMeta();
			if (enabled)
				meta.addEnchant(Main.getInstance().getItemManager().glowEnchantment, 0, false);
			else
				meta.removeEnchant(Main.getInstance().getItemManager().glowEnchantment);
			item.setItemMeta(meta);
		}
		getInventory().setArmorContents(armorContents);
		updateInventory();
	}

	public int getSecondsLeftOnKingTool()
	{
		if (!isKing())
			return -1;
		if (lastKingToolUseTime == -1)
			return -1;

		long millis = System.currentTimeMillis();
		int secondsLeft = 20 - (int) (Math.floor((millis - lastKingToolUseTime) / 1000f));
		if (secondsLeft <= 0)
			return -1;
		return secondsLeft;
	}

	public void setLastDamager(Player player)
	{
		if (player != null)
		{
			if (!player.getUniqueId().equals(getUniqueId()))
			{
				lastPlayerToAttack = player.getUniqueId();
				lastPlayerAttack = System.currentTimeMillis();
			}
		} else
		{
			lastPlayerAttack = -1;
			lastPlayerToAttack = null;
		}
	}

	public Player getLastDamager()
	{
		Player damager = null;
		if (lastPlayerToAttack != null && System.currentTimeMillis() < lastPlayerAttack + 15 * 1000)
			return Bukkit.getPlayer(lastPlayerToAttack);
		return null;
	}

	public void attackedPlayer()
	{
		setLastAttack(System.currentTimeMillis());
	}

	public void setLastAttack(long millis)
	{
		lastAttack = millis;
	}

	public long getLastAttack()
	{
		return lastAttack;
	}

	public boolean outOfCombat()
	{
		long millis = System.currentTimeMillis();
		return millis > lastAttack + 15 * 1000l && getLastDamager() == null;
	}

	public void addKill(Player player)
	{
		addKill(player, true);
	}

	public void addKill(Player player, boolean sendMessage)
	{
		if (!player.getUniqueId().equals(this.player.getUniqueId()))
		{
			boolean isKing = Main.getInstance().getGameManager().isKing(player);
			int points = 10;
			if (isKing)
				points *= 3;
			if (sendMessage)
			{
				this.player.sendMessage(ChatColor.YELLOW + "You killed " + player.getName() + "!");
				this.player.sendMessage(ChatColor.GOLD + " + " + points + " points");
				playKillSound();
			}
			this.tokens += points;
			kills++;
		}
	}

	public void playKillSound()
	{
		this.player.playSound(this.player.getLocation(), Sound.SUCCESSFUL_HIT, 1f, 2f);
	}

	public void addDeath()
	{
		deaths++;
	}

	public void setKills(int amount)
	{
		kills = amount;
	}

	public int getKills(boolean all)
	{
		return kills + (all ? perm_kills : 0);
	}

	public void setDeaths(int amount)
	{
		deaths = amount;
	}

	public int getDeaths(boolean all)
	{
		return deaths + (all ? perm_deaths : 0);
	}

	public void addTokens(int amount)
	{
		tokens += amount;
	}

	public void removeTokens(int amount)
	{
		tokens -= amount;
	}

	public void setTokens(int amount)
	{
		tokens = amount;
	}

	public int getTokens()
	{
		return tokens;
	}

	public int getPermKills()
	{
		return perm_kills;
	}

	public int getPermDeaths()
	{
		return perm_deaths;
	}

	public void setPermKills(int permKills)
	{
		perm_kills = permKills;
	}

	public void setPermDeaths(int permDeaths)
	{
		perm_deaths = permDeaths;
	}

	public CrownType getCrownType()
	{
		return crownType;
	}

	public void setCrownType(CrownType crownType)
	{
		this.crownType = crownType;
	}

	public boolean isKing()
	{
		return Main.getInstance().getGameManager().isKing(player);
	}

	public Location getLastSpawn()
	{
		return lastSpawn;
	}

	public void setLastSpawn(Location lastSpawn)
	{
		this.lastSpawn = lastSpawn;
	}

	public long getLastShopMessage()
	{
		return lastShopMessage;
	}

	public void setLastShopMessage(long lastShopMessage)
	{
		this.lastShopMessage = lastShopMessage;
	}

	public void abandonConversation(Conversation arg0, ConversationAbandonedEvent arg1)
	{
		player.abandonConversation(arg0, arg1);
	}

	public void abandonConversation(Conversation arg0)
	{
		player.abandonConversation(arg0);
	}

	public void acceptConversationInput(String arg0)
	{
		player.acceptConversationInput(arg0);
	}

	public PermissionAttachment addAttachment(Plugin arg0, int arg1)
	{
		return player.addAttachment(arg0, arg1);
	}

	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2, int arg3)
	{
		return player.addAttachment(arg0, arg1, arg2, arg3);
	}

	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2)
	{
		return player.addAttachment(arg0, arg1, arg2);
	}

	public PermissionAttachment addAttachment(Plugin arg0)
	{
		return player.addAttachment(arg0);
	}

	public boolean addPotionEffect(PotionEffect arg0, boolean arg1)
	{
		return player.addPotionEffect(arg0, arg1);
	}

	public boolean addPotionEffect(PotionEffect arg0)
	{
		return player.addPotionEffect(arg0);
	}

	public boolean addPotionEffects(Collection<PotionEffect> arg0)
	{
		return player.addPotionEffects(arg0);
	}

	public void awardAchievement(Achievement arg0)
	{
		player.awardAchievement(arg0);
	}

	public boolean beginConversation(Conversation arg0)
	{
		return player.beginConversation(arg0);
	}

	public boolean canSee(Player arg0)
	{
		return player.canSee(arg0);
	}

	public void chat(String arg0)
	{
		player.chat(arg0);
	}

	public void closeInventory()
	{
		player.closeInventory();
	}

	public void damage(double arg0, Entity arg1)
	{
		player.damage(arg0, arg1);
	}

	public void damage(double arg0)
	{
		player.damage(arg0);
	}

	public void decrementStatistic(Statistic arg0, EntityType arg1, int arg2)
	{
		player.decrementStatistic(arg0, arg1, arg2);
	}

	public void decrementStatistic(Statistic arg0, EntityType arg1) throws IllegalArgumentException
	{
		player.decrementStatistic(arg0, arg1);
	}

	public void decrementStatistic(Statistic arg0, int arg1) throws IllegalArgumentException
	{
		player.decrementStatistic(arg0, arg1);
	}

	public void decrementStatistic(Statistic arg0, Material arg1, int arg2) throws IllegalArgumentException
	{
		player.decrementStatistic(arg0, arg1, arg2);
	}

	public void decrementStatistic(Statistic arg0, Material arg1) throws IllegalArgumentException
	{
		player.decrementStatistic(arg0, arg1);
	}

	public void decrementStatistic(Statistic arg0) throws IllegalArgumentException
	{
		player.decrementStatistic(arg0);
	}

	public boolean eject()
	{
		return player.eject();
	}

	public Collection<PotionEffect> getActivePotionEffects()
	{
		return player.getActivePotionEffects();
	}

	public InetSocketAddress getAddress()
	{
		return player.getAddress();
	}

	public boolean getAllowFlight()
	{
		return player.getAllowFlight();
	}

	public Location getBedSpawnLocation()
	{
		return player.getBedSpawnLocation();
	}

	public boolean getCanPickupItems()
	{
		return player.getCanPickupItems();
	}

	public Location getCompassTarget()
	{
		return player.getCompassTarget();
	}

	public String getCustomName()
	{
		return player.getCustomName();
	}

	public String getDisplayName()
	{
		return player.getDisplayName();
	}

	public Set<PermissionAttachmentInfo> getEffectivePermissions()
	{
		return player.getEffectivePermissions();
	}

	public Inventory getEnderChest()
	{
		return player.getEnderChest();
	}

	public int getEntityId()
	{
		return player.getEntityId();
	}

	public EntityEquipment getEquipment()
	{
		return player.getEquipment();
	}

	public float getExhaustion()
	{
		return player.getExhaustion();
	}

	public float getExp()
	{
		return player.getExp();
	}

	public int getExpToLevel()
	{
		return player.getExpToLevel();
	}

	public double getEyeHeight()
	{
		return player.getEyeHeight();
	}

	public double getEyeHeight(boolean arg0)
	{
		return player.getEyeHeight(arg0);
	}

	public Location getEyeLocation()
	{
		return player.getEyeLocation();
	}

	public float getFallDistance()
	{
		return player.getFallDistance();
	}

	public int getFireTicks()
	{
		return player.getFireTicks();
	}

	public long getFirstPlayed()
	{
		return player.getFirstPlayed();
	}

	public float getFlySpeed()
	{
		return player.getFlySpeed();
	}

	public int getFoodLevel()
	{
		return player.getFoodLevel();
	}

	public GameMode getGameMode()
	{
		return player.getGameMode();
	}

	public double getHealth()
	{
		return player.getHealth();
	}

	public double getHealthScale()
	{
		return player.getHealthScale();
	}

	public PlayerInventory getInventory()
	{
		return player.getInventory();
	}

	public ItemStack getItemInHand()
	{
		return player.getItemInHand();
	}

	public ItemStack getItemOnCursor()
	{
		return player.getItemOnCursor();
	}

	public Player getKiller()
	{
		return player.getKiller();
	}

	public double getLastDamage()
	{
		return player.getLastDamage();
	}

	public EntityDamageEvent getLastDamageCause()
	{
		return player.getLastDamageCause();
	}

	public long getLastPlayed()
	{
		return player.getLastPlayed();
	}

	public List<Block> getLastTwoTargetBlocks(HashSet<Byte> arg0, int arg1)
	{
		return player.getLastTwoTargetBlocks(arg0, arg1);
	}

	public List<Block> getLastTwoTargetBlocks(Set<Material> arg0, int arg1)
	{
		return player.getLastTwoTargetBlocks(arg0, arg1);
	}

	public Entity getLeashHolder() throws IllegalStateException
	{
		return player.getLeashHolder();
	}

	public int getLevel()
	{
		return player.getLevel();
	}

	public List<Block> getLineOfSight(HashSet<Byte> arg0, int arg1)
	{
		return player.getLineOfSight(arg0, arg1);
	}

	public List<Block> getLineOfSight(Set<Material> arg0, int arg1)
	{
		return player.getLineOfSight(arg0, arg1);
	}

	public Set<String> getListeningPluginChannels()
	{
		return player.getListeningPluginChannels();
	}

	public Location getLocation()
	{
		return player.getLocation();
	}

	public Location getLocation(Location arg0)
	{
		return player.getLocation(arg0);
	}

	public int getMaxFireTicks()
	{
		return player.getMaxFireTicks();
	}

	public double getMaxHealth()
	{
		return player.getMaxHealth();
	}

	public int getMaximumAir()
	{
		return player.getMaximumAir();
	}

	public int getMaximumNoDamageTicks()
	{
		return player.getMaximumNoDamageTicks();
	}

	public List<MetadataValue> getMetadata(String arg0)
	{
		return player.getMetadata(arg0);
	}

	public String getName()
	{
		return player.getName();
	}

	public List<Entity> getNearbyEntities(double arg0, double arg1, double arg2)
	{
		return player.getNearbyEntities(arg0, arg1, arg2);
	}

	public int getNoDamageTicks()
	{
		return player.getNoDamageTicks();
	}

	public InventoryView getOpenInventory()
	{
		return player.getOpenInventory();
	}

	public Entity getPassenger()
	{
		return player.getPassenger();
	}

	public String getPlayerListName()
	{
		return player.getPlayerListName();
	}

	public long getPlayerTime()
	{
		return player.getPlayerTime();
	}

	public long getPlayerTimeOffset()
	{
		return player.getPlayerTimeOffset();
	}

	public WeatherType getPlayerWeather()
	{
		return player.getPlayerWeather();
	}

	public int getRemainingAir()
	{
		return player.getRemainingAir();
	}

	public boolean getRemoveWhenFarAway()
	{
		return player.getRemoveWhenFarAway();
	}

	public float getSaturation()
	{
		return player.getSaturation();
	}

	public Scoreboard getScoreboard()
	{
		return player.getScoreboard();
	}

	public Server getServer()
	{
		return player.getServer();
	}

	public int getSleepTicks()
	{
		return player.getSleepTicks();
	}

	public int getStatistic(Statistic arg0, EntityType arg1) throws IllegalArgumentException
	{
		return player.getStatistic(arg0, arg1);
	}

	public int getStatistic(Statistic arg0, Material arg1) throws IllegalArgumentException
	{
		return player.getStatistic(arg0, arg1);
	}

	public int getStatistic(Statistic arg0) throws IllegalArgumentException
	{
		return player.getStatistic(arg0);
	}

	public Block getTargetBlock(HashSet<Byte> arg0, int arg1)
	{
		return player.getTargetBlock(arg0, arg1);
	}

	public Block getTargetBlock(Set<Material> arg0, int arg1)
	{
		return player.getTargetBlock(arg0, arg1);
	}

	public int getTicksLived()
	{
		return player.getTicksLived();
	}

	public int getTotalExperience()
	{
		return player.getTotalExperience();
	}

	public EntityType getType()
	{
		return player.getType();
	}

	public UUID getUniqueId()
	{
		return player.getUniqueId();
	}

	public Entity getVehicle()
	{
		return player.getVehicle();
	}

	public Vector getVelocity()
	{
		return player.getVelocity();
	}

	public float getWalkSpeed()
	{
		return player.getWalkSpeed();
	}

	public World getWorld()
	{
		return player.getWorld();
	}

	public void giveExp(int arg0)
	{
		player.giveExp(arg0);
	}

	public void giveExpLevels(int arg0)
	{
		player.giveExpLevels(arg0);
	}

	public boolean hasAchievement(Achievement arg0)
	{
		return player.hasAchievement(arg0);
	}

	public boolean hasLineOfSight(Entity arg0)
	{
		return player.hasLineOfSight(arg0);
	}

	public boolean hasMetadata(String arg0)
	{
		return player.hasMetadata(arg0);
	}

	public boolean hasPermission(Permission arg0)
	{
		return player.hasPermission(arg0);
	}

	public boolean hasPermission(String arg0)
	{
		return player.hasPermission(arg0);
	}

	public boolean hasPlayedBefore()
	{
		return player.hasPlayedBefore();
	}

	public boolean hasPotionEffect(PotionEffectType arg0)
	{
		return player.hasPotionEffect(arg0);
	}

	public void hidePlayer(Player arg0)
	{
		player.hidePlayer(arg0);
	}

	public void incrementStatistic(Statistic arg0, EntityType arg1, int arg2) throws IllegalArgumentException
	{
		player.incrementStatistic(arg0, arg1, arg2);
	}

	public void incrementStatistic(Statistic arg0, EntityType arg1) throws IllegalArgumentException
	{
		player.incrementStatistic(arg0, arg1);
	}

	public void incrementStatistic(Statistic arg0, int arg1) throws IllegalArgumentException
	{
		player.incrementStatistic(arg0, arg1);
	}

	public void incrementStatistic(Statistic arg0, Material arg1, int arg2) throws IllegalArgumentException
	{
		player.incrementStatistic(arg0, arg1, arg2);
	}

	public void incrementStatistic(Statistic arg0, Material arg1) throws IllegalArgumentException
	{
		player.incrementStatistic(arg0, arg1);
	}

	public void incrementStatistic(Statistic arg0) throws IllegalArgumentException
	{
		player.incrementStatistic(arg0);
	}

	public boolean isBanned()
	{
		return player.isBanned();
	}

	public boolean isBlocking()
	{
		return player.isBlocking();
	}

	public boolean isConversing()
	{
		return player.isConversing();
	}

	public boolean isCustomNameVisible()
	{
		return player.isCustomNameVisible();
	}

	public boolean isDead()
	{
		return player.isDead();
	}

	public boolean isEmpty()
	{
		return player.isEmpty();
	}

	public boolean isFlying()
	{
		return player.isFlying();
	}

	public boolean isHealthScaled()
	{
		return player.isHealthScaled();
	}

	public boolean isInsideVehicle()
	{
		return player.isInsideVehicle();
	}

	public boolean isLeashed()
	{
		return player.isLeashed();
	}

	public boolean isOnGround()
	{
		return player.isOnGround();
	}

	public boolean isOnline()
	{
		return player.isOnline();
	}

	public boolean isOp()
	{
		return player.isOp();
	}

	public boolean isPermissionSet(Permission arg0)
	{
		return player.isPermissionSet(arg0);
	}

	public boolean isPermissionSet(String arg0)
	{
		return player.isPermissionSet(arg0);
	}

	public boolean isPlayerTimeRelative()
	{
		return player.isPlayerTimeRelative();
	}

	public boolean isSleeping()
	{
		return player.isSleeping();
	}

	public boolean isSleepingIgnored()
	{
		return player.isSleepingIgnored();
	}

	public boolean isSneaking()
	{
		return player.isSneaking();
	}

	public boolean isSprinting()
	{
		return player.isSprinting();
	}

	public boolean isValid()
	{
		return player.isValid();
	}

	public boolean isWhitelisted()
	{
		return player.isWhitelisted();
	}

	public void kickPlayer(String arg0)
	{
		player.kickPlayer(arg0);
	}

	public <T extends Projectile> T launchProjectile(Class<? extends T> arg0, Vector arg1)
	{
		return player.launchProjectile(arg0, arg1);
	}

	public <T extends Projectile> T launchProjectile(Class<? extends T> arg0)
	{
		return player.launchProjectile(arg0);
	}

	public boolean leaveVehicle()
	{
		return player.leaveVehicle();
	}

	public void loadData()
	{
		player.loadData();
	}

	public InventoryView openEnchanting(Location arg0, boolean arg1)
	{
		return player.openEnchanting(arg0, arg1);
	}

	public InventoryView openInventory(Inventory arg0)
	{
		return player.openInventory(arg0);
	}

	public void openInventory(InventoryView arg0)
	{
		player.openInventory(arg0);
	}

	public InventoryView openWorkbench(Location arg0, boolean arg1)
	{
		return player.openWorkbench(arg0, arg1);
	}

	public boolean performCommand(String arg0)
	{
		return player.performCommand(arg0);
	}

	public void playEffect(EntityEffect arg0)
	{
		player.playEffect(arg0);
	}

	public void playEffect(Location arg0, Effect arg1, int arg2)
	{
		player.playEffect(arg0, arg1, arg2);
	}

	public <T> void playEffect(Location arg0, Effect arg1, T arg2)
	{
		player.playEffect(arg0, arg1, arg2);
	}

	public void playNote(Location arg0, byte arg1, byte arg2)
	{
		player.playNote(arg0, arg1, arg2);
	}

	public void playNote(Location arg0, Instrument arg1, Note arg2)
	{
		player.playNote(arg0, arg1, arg2);
	}

	public void playSound(Location arg0, Sound arg1, float arg2, float arg3)
	{
		player.playSound(arg0, arg1, arg2, arg3);
	}

	public void playSound(Location arg0, String arg1, float arg2, float arg3)
	{
		player.playSound(arg0, arg1, arg2, arg3);
	}

	public void recalculatePermissions()
	{
		player.recalculatePermissions();
	}

	public void remove()
	{
		player.remove();
	}

	public void removeAchievement(Achievement arg0)
	{
		player.removeAchievement(arg0);
	}

	public void removeAttachment(PermissionAttachment arg0)
	{
		player.removeAttachment(arg0);
	}

	public void removeMetadata(String arg0, Plugin arg1)
	{
		player.removeMetadata(arg0, arg1);
	}

	public void removePotionEffect(PotionEffectType arg0)
	{
		player.removePotionEffect(arg0);
	}

	public void resetMaxHealth()
	{
		player.resetMaxHealth();
	}

	public void resetPlayerTime()
	{
		player.resetPlayerTime();
	}

	public void resetPlayerWeather()
	{
		player.resetPlayerWeather();
	}

	public void saveData()
	{
		player.saveData();
	}

	public void sendBlockChange(Location arg0, int arg1, byte arg2)
	{
		player.sendBlockChange(arg0, arg1, arg2);
	}

	public void sendBlockChange(Location arg0, Material arg1, byte arg2)
	{
		player.sendBlockChange(arg0, arg1, arg2);
	}

	public boolean sendChunkChange(Location arg0, int arg1, int arg2, int arg3, byte[] arg4)
	{
		return player.sendChunkChange(arg0, arg1, arg2, arg3, arg4);
	}

	public void sendMap(MapView arg0)
	{
		player.sendMap(arg0);
	}

	public void sendMessage(String arg0)
	{
		player.sendMessage(arg0);
	}

	public void sendMessage(String[] arg0)
	{
		player.sendMessage(arg0);
	}

	public void sendPluginMessage(Plugin arg0, String arg1, byte[] arg2)
	{
		player.sendPluginMessage(arg0, arg1, arg2);
	}

	public void sendRawMessage(String arg0)
	{
		player.sendRawMessage(arg0);
	}

	public void sendSignChange(Location arg0, String[] arg1) throws IllegalArgumentException
	{
		player.sendSignChange(arg0, arg1);
	}

	public Map<String, Object> serialize()
	{
		return player.serialize();
	}

	public void setAllowFlight(boolean arg0)
	{
		player.setAllowFlight(arg0);
	}

	public void setBanned(boolean arg0)
	{
		player.setBanned(arg0);
	}

	public void setBedSpawnLocation(Location arg0, boolean arg1)
	{
		player.setBedSpawnLocation(arg0, arg1);
	}

	public void setBedSpawnLocation(Location arg0)
	{
		player.setBedSpawnLocation(arg0);
	}

	public void setCanPickupItems(boolean arg0)
	{
		player.setCanPickupItems(arg0);
	}

	public void setCompassTarget(Location arg0)
	{
		player.setCompassTarget(arg0);
	}

	public void setCustomName(String arg0)
	{
		player.setCustomName(arg0);
	}

	public void setCustomNameVisible(boolean arg0)
	{
		player.setCustomNameVisible(arg0);
	}

	public void setDisplayName(String arg0)
	{
		player.setDisplayName(arg0);
	}

	public void setExhaustion(float arg0)
	{
		player.setExhaustion(arg0);
	}

	public void setExp(float arg0)
	{
		player.setExp(arg0);
	}

	public void setFallDistance(float arg0)
	{
		player.setFallDistance(arg0);
	}

	public void setFireTicks(int arg0)
	{
		player.setFireTicks(arg0);
	}

	public void setFlySpeed(float arg0) throws IllegalArgumentException
	{
		player.setFlySpeed(arg0);
	}

	public void setFlying(boolean arg0)
	{
		player.setFlying(arg0);
	}

	public void setFoodLevel(int arg0)
	{
		player.setFoodLevel(arg0);
	}

	public void setGameMode(GameMode arg0)
	{
		player.setGameMode(arg0);
	}

	public void setHealth(double arg0)
	{
		player.setHealth(arg0);
	}

	public void setHealthScale(double arg0) throws IllegalArgumentException
	{
		player.setHealthScale(arg0);
	}

	public void setHealthScaled(boolean arg0)
	{
		player.setHealthScaled(arg0);
	}

	public void setItemInHand(ItemStack arg0)
	{
		player.setItemInHand(arg0);
	}

	public void setItemOnCursor(ItemStack arg0)
	{
		player.setItemOnCursor(arg0);
	}

	public void setLastDamage(double arg0)
	{
		player.setLastDamage(arg0);
	}

	public void setLastDamageCause(EntityDamageEvent arg0)
	{
		player.setLastDamageCause(arg0);
	}

	public boolean setLeashHolder(Entity arg0)
	{
		return player.setLeashHolder(arg0);
	}

	public void setLevel(int arg0)
	{
		player.setLevel(arg0);
	}

	public void setMaxHealth(double arg0)
	{
		player.setMaxHealth(arg0);
	}

	public void setMaximumAir(int arg0)
	{
		player.setMaximumAir(arg0);
	}

	public void setMaximumNoDamageTicks(int arg0)
	{
		player.setMaximumNoDamageTicks(arg0);
	}

	public void setMetadata(String arg0, MetadataValue arg1)
	{
		player.setMetadata(arg0, arg1);
	}

	public void setNoDamageTicks(int arg0)
	{
		player.setNoDamageTicks(arg0);
	}

	public void setOp(boolean arg0)
	{
		player.setOp(arg0);
	}

	public boolean setPassenger(Entity arg0)
	{
		return player.setPassenger(arg0);
	}

	public void setPlayerListName(String arg0)
	{
		player.setPlayerListName(arg0);
	}

	public void setPlayerTime(long arg0, boolean arg1)
	{
		player.setPlayerTime(arg0, arg1);
	}

	public void setPlayerWeather(WeatherType arg0)
	{
		player.setPlayerWeather(arg0);
	}

	public void setRemainingAir(int arg0)
	{
		player.setRemainingAir(arg0);
	}

	public void setRemoveWhenFarAway(boolean arg0)
	{
		player.setRemoveWhenFarAway(arg0);
	}

	public void setResourcePack(String arg0)
	{
		player.setResourcePack(arg0);
	}

	public void setSaturation(float arg0)
	{
		player.setSaturation(arg0);
	}

	public void setScoreboard(Scoreboard arg0) throws IllegalArgumentException, IllegalStateException
	{
		player.setScoreboard(arg0);
	}

	public void setSleepingIgnored(boolean arg0)
	{
		player.setSleepingIgnored(arg0);
	}

	public void setSneaking(boolean arg0)
	{
		player.setSneaking(arg0);
	}

	public void setSpectatorTarget(Entity arg0)
	{
		player.setSpectatorTarget(arg0);
	}

	public void setSprinting(boolean arg0)
	{
		player.setSprinting(arg0);
	}

	public void setStatistic(Statistic arg0, EntityType arg1, int arg2)
	{
		player.setStatistic(arg0, arg1, arg2);
	}

	public void setStatistic(Statistic arg0, int arg1) throws IllegalArgumentException
	{
		player.setStatistic(arg0, arg1);
	}

	public void setStatistic(Statistic arg0, Material arg1, int arg2) throws IllegalArgumentException
	{
		player.setStatistic(arg0, arg1, arg2);
	}

	public void setTexturePack(String arg0)
	{
		player.setTexturePack(arg0);
	}

	public void setTicksLived(int arg0)
	{
		player.setTicksLived(arg0);
	}

	public void setTotalExperience(int arg0)
	{
		player.setTotalExperience(arg0);
	}

	public void setVelocity(Vector arg0)
	{
		player.setVelocity(arg0);
	}

	public void setWalkSpeed(float arg0) throws IllegalArgumentException
	{
		player.setWalkSpeed(arg0);
	}

	public void setWhitelisted(boolean arg0)
	{
		player.setWhitelisted(arg0);
	}

	public boolean setWindowProperty(Property arg0, int arg1)
	{
		return player.setWindowProperty(arg0, arg1);
	}

	public Arrow shootArrow()
	{
		return player.shootArrow();
	}

	public void showPlayer(Player arg0)
	{
		player.showPlayer(arg0);
	}

	public Spigot spigot()
	{
		return player.spigot();
	}

	public boolean teleport(Entity arg0, TeleportCause arg1)
	{
		return player.teleport(arg0, arg1);
	}

	public boolean teleport(Entity arg0)
	{
		return player.teleport(arg0);
	}

	public boolean teleport(Location arg0, TeleportCause arg1)
	{
		return player.teleport(arg0, arg1);
	}

	public boolean teleport(Location arg0)
	{
		return player.teleport(arg0);
	}

	public Egg throwEgg()
	{
		return player.throwEgg();
	}

	public Snowball throwSnowball()
	{
		return player.throwSnowball();
	}

	public void updateInventory()
	{
		player.updateInventory();
	}

	@Override
	public Player getPlayer()
	{
		return player;
	}

	public ItemStack getHead()
	{
		return head;
	}
}
