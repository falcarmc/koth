package net.falcarmc.kotah.commands;

import net.falcarmc.kotah.managers.MessageManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public abstract class FACommand implements CommandExecutor
{
	public FACommand(boolean staff)
	{
		this.staffPerm = staff;
		String className = getClass().getSimpleName();
		this.command = className.substring(0, className.indexOf("Command")).toLowerCase();
		perm = "falcar." + (staffPerm ? "staff." : "user.") + command;
	}
	
	public abstract void run(CommandSender cs, String[] args);
	
	protected CommandSender cs;
	protected boolean staffPerm;
	protected String command;
	protected String perm; 
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
	{
		this.cs = cs;
		if(hasPermission())
		{
			run(cs, args);
			return true;
		}
		return false;
	}
	
	public boolean hasPermission()
	{
		boolean hasPermission = cs.hasPermission(perm);
		if(!hasPermission)
			cs.sendMessage(String.format(MessageManager.NO_PERM, perm));
		return hasPermission;
	}
	
	public String getPermission()
	{
		return perm;
	}

	public boolean isPlayer()
	{
		boolean isPlayer = cs instanceof Player;
		if(!isPlayer)
			cs.sendMessage(MessageManager.PLAYER_ONLY);
		return isPlayer;
	}
	
	public boolean isConsole()
	{
		return cs instanceof ConsoleCommandSender;
	}
	
	public void sendUnformatedMessage(String mes)
	{
		cs.sendMessage(mes);
	}
	
	public void sendMessage(String mes)
	{
		cs.sendMessage(MessageManager.LOGO + mes);
	}

	public Player getPlayer(CommandSender sender, String playerName)
	{
		Player player = getPlayer(playerName);
		if (player == null)
		{
			sender.sendMessage(MessageManager.LOGO + "Cannot find player \"" + playerName + "\". Please ensure you have spelt their name correctly.");
		}
		return player;
	}

	public Player getPlayer(String playerName)
	{
		Player player = Bukkit.getPlayer(playerName);
		if (player == null && (player = getPlayerFromDisplayName(playerName)) == null)
		{
			return null;
		}
		return player;
	}

	public Player getPlayerFromDisplayName(String partialDisplayName)
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			if (ChatColor.stripColor(player.getDisplayName()).toLowerCase().contains(partialDisplayName.toLowerCase()))
			{
				return player;
			}
		}
		return null;
	}
	
	public void sendUsage(String arg, String info)
	{
		sendMessage(String.format(MessageManager.USAGE, command, arg, info));
	}

	public void usage(String params)
	{
		cs.sendMessage(MessageManager.LOGO + "Usage: /" + (command != null ? command : "null") + " " + params);
	}

	public String arrayString(String[] array, int startIndex)
	{
		return arrayString(array, startIndex, " ");
	}

	public String arrayString(String[] array, int startIndex, String seperator)
	{
		StringBuilder output = new StringBuilder();
		for (int i = startIndex; i < array.length; i++)
		{
			output.append(array[i] + (i != array.length - 1 ? seperator : ""));
		}
		return output.toString();
	}
}
