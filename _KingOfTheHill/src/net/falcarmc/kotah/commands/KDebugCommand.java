package net.falcarmc.kotah.commands;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.managers.GameManager;

import org.bukkit.command.CommandSender;

public class KDebugCommand extends FACommand
{
	public KDebugCommand()
	{
		super(true);
	}

	@Override
	public void run(CommandSender cs, String[] args)
	{
		GameManager gM = Main.getInstance().getGameManager();
		sendMessage("kingUUID: " + gM.getKingUUID());
		sendMessage("kingPlayer name: " + (gM.getKing() != null ? gM.getKing().getName() : null));
		sendMessage("king seconds: " + (int)gM.getKingDuration());
		sendMessage("activePlayers: " + gM.getActivePlayers());
	}
}
