package net.falcarmc.kotah.commands;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.User;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShopCommand extends FACommand
{
	public ShopCommand()
	{
		super(false);
	}

	@Override
	public void run(CommandSender cs, String[] args)
	{
		if(isPlayer())
		{
			Player player = (Player) cs;
			User user = Main.getInstance().getUserManager().getUser(player);
			if(user.isInvincible())
			{
				user.setInvincible(false);
				user.teleport(Main.getInstance().getShopManager().insideShopLocation);
			}
			else
			{
				user.sendMessage(ChatColor.YELLOW + "You must still be invincible to use /shop.");
			}
		}
	}
}
