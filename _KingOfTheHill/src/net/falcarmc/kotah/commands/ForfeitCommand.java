package net.falcarmc.kotah.commands;

import java.util.ArrayList;
import java.util.List;

import net.falcarmc.kotah.Main;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ForfeitCommand extends FACommand
{
	public ForfeitCommand()
	{
		super(false);
	}

	@Override
	public void run(CommandSender cs, String[] args)
	{
		if(isPlayer())
		{
			Player player = (Player) cs;
			if(Main.getInstance().getGameManager().isKing(player))
			{
				int secondsAsKing = (int)Main.getInstance().getGameManager().getKingDuration();
				if(secondsAsKing >= 60)
				{
					List<Player> players = new ArrayList<>(Main.getInstance().getGameManager().getActivePlayers());
					if(players.size() > 1)
					{
						players.remove(player);
						int index = Main.getInstance().random.nextInt(players.size());
						Player newKing = players.get(index);
						Main.getInstance().getGameManager().setKing(newKing, player.getName() + " has forfeited the crown.");
					}
					else
					{
						sendMessage("There is no one else active in the game to give the crown to.");
					}
				}
				else
				{
					int secondsRemaining = 60 - secondsAsKing;
					sendMessage("You need to be the king for at least 1 minute before using this command. " + secondsRemaining + "s remaining.");
				}
			}
			else
			{
				sendMessage("You must be the king (or queen) to perform this command.");
			}
		}
	}
}
