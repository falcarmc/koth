package net.falcarmc.kotah.commands;

import net.falcarmc.kotah.Main;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateCommand extends FACommand
{
	public CreateCommand()
	{
		super(true);
	}

	@Override
	public void run(CommandSender cs, String[] args)
	{
		if(!isPlayer())
			return;

		Player player = (Player) cs;
		if(args.length > 0 && args[0].equalsIgnoreCase("crown"))
		{
			Main.getInstance().getCrownManager().createCrown(new Location(Main.getInstance().mainWorld, 233.5f, 27.4f, 53.5f), true);
		} else if (args.length > 0 && args[0].equalsIgnoreCase("cheat"))
		{
			Main.getInstance().getGameManager().setKing(player, player.getName() + " used a cheat code! Oi!");
		}
		else
		{
			usage("<crown | cheat>");
		}
	}
}
