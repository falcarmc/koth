package net.falcarmc.kotah.runnables;

import net.falcarmc.kotah.Main;
import net.falcarmc.kotah.PlayerEntity;
import net.falcarmc.kotah.PlayerEntity.PEntType;
import net.falcarmc.kotah.entities.EntityRocket;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.scheduler.BukkitRunnable;

public class OneTickRunnable extends BukkitRunnable
{
	@Override
	public void run()
	{
		Main.getInstance().getShopManager().rotateStand();
		for (PlayerEntity ent : Main.getInstance().getEntityManager().createdEntities)
		{
			if (ent.getType() == PEntType.ROCKET)
			{
				EntityRocket rocket = (EntityRocket) ((CraftEntity) ent.getEntity()).getHandle();
				rocket.showParticles();
			}
			
			
		}
	}
}
