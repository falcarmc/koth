package net.falcarmc.kotah.entities;

import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.GenericAttributes;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class ZombieParticle
{
	private Vector direction;
	private Location location;

	public ZombieParticle(Vector direction, Location location)
	{
		this.direction = direction;
		this.location = location;
	}

	public void move()
	{
		// TODO: CREATE PARTICLE IN PREVIOUS LOCATION
		location.add(direction);
		Block block = location.getBlock();
		if (block.getType() != Material.AIR)
		{
			spawnZombie(location);
		}
	}

	public Zombie spawnZombie(Location location)
	{
		Block block = location.getBlock();
		for (int y = block.getLocation().getBlockY(); y < 255; y++)
		{
			if (block.getType() != Material.AIR)
				block = block.getRelative(0, 1, 0);
			else
				break;
		}
		Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);
		zombie.setBaby(false);
		if (zombie.getVehicle() != null)
			zombie.getVehicle().remove();
		zombie.setVillager(false);
		zombie.getEquipment().setItemInHand(new ItemStack(Material.AIR));
		Bukkit.broadcastMessage(zombie.getEquipment().getItemInHand().getType().name());
		zombie.setCanPickupItems(false);
		((EntityLiving) ((CraftEntity) zombie).getHandle()).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.15);
		((EntityLiving) ((CraftEntity) zombie).getHandle()).getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(30);
		zombie.setHealth(1);
		return zombie;
	}
}
