package net.falcarmc.kotah.entities;

import net.falcarmc.kotah.Main;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntityWitherSkull;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_8_R3.World;
import net.royawesome.jlibnoise.MathHelper;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class EntityRocket extends EntityWitherSkull
{
	int ticks = 0;
	int soundDistance = 35;

	public EntityRocket(World world)
	{
		super(world);
	}

	public EntityRocket(World world, EntityLiving entityliving, double d0, double d1, double d2)
	{
		super(world, entityliving, d0, d1, d2);
	}

	@Override
	public void move(double d1, double d2, double d3)
	{
		super.move(d1, d2, d3);
	}

	public void showParticles()
	{
		world.addParticle(EnumParticle.HEART, locX, locY, locZ, 1.0D, 0.0D, 0.0D, new int[0]);
		updateLocation();
		// setPosition(locX, locY, locZ);

		Location loc = new Location(Main.getInstance().mainWorld, locX, locY, locZ);
		if (ticks % 5 == 0)
		{
			Vector rocketVector = loc.toVector();
			float pitch = 0.6f + Main.getInstance().random.nextFloat() * 0.2f;
			for (Player p : loc.getWorld().getPlayers())
			{
				Vector playerVector = p.getLocation().toVector();
				Location soundLocation = p.getLocation().clone().add(playerVector.clone().subtract(rocketVector).normalize().multiply(-10));
				float distance = (float) p.getLocation().distance(loc);
				if (distance < soundDistance)
				{
					float volume = (float) ((soundDistance - distance) / soundDistance);
					p.playSound(soundLocation, Sound.WITHER_SHOOT, volume, pitch);
				}
			}

			// for(PlayerEntity entity : Main.getInstance().getEntityManager().createdEntities)
			// {
			// Entity e = entity.getEntity();
			// if(entity.getType() == PEntType.ROCKET && entity.getEntity().getEntityId() != getId())
			// {
			// if(e.getLocation().distanceSquared(loc) < 3)
			// {
			// Main.getInstance().getGameManager().witherSkullHit(entity, entity.getEntity().getLocation());
			// }
			// }
			// }
		}
		// Main.plugin.getLogger().info(locX + ", " + locY + ", " + locZ);
		ticks++;
	}

	public void updateLocation()
	{
		int i = this.getId();
		int j = MathHelper.floor((locX + (dirX * j())) * 32d);
		int k = MathHelper.floor((locY + (dirY * j())) * 32d);
		int l = MathHelper.floor((locZ + (dirZ * j())) * 32d);
		byte b0 = (byte) ((int) (yaw * 256.0F / 360.0F));
		byte b1 = (byte) ((int) (pitch * 256.0F / 360.0F));

		PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport(i, j, k, l, b0, b1, true);
		for (Player player : Bukkit.getOnlinePlayers())
		{
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}

	@Override
	protected float j()
	{
		return 0.81f; // .83
	}
}
