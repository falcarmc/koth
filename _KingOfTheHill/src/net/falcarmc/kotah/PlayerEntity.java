package net.falcarmc.kotah;

import org.bukkit.entity.Entity;

public class PlayerEntity
{
	public enum PEntType
	{
		ZOMBIE, ROCKET
	}
	
	Entity entity;
	User user;
	int ticksUntilPurged;
	PEntType type;
	
	public PlayerEntity(Entity anEntity, PEntType type, User aUser, int ticksUntilPurged)
	{
		entity = anEntity;
		this.type = type;
		user = aUser;
		this.ticksUntilPurged = ticksUntilPurged;
	}

	public Entity getEntity()
	{
		return entity;
	}

	public void setEntity(Entity entity)
	{
		this.entity = entity;
	}
	
	public PEntType getType()
	{
		return type;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public int getTicksUntilPurged()
	{
		return ticksUntilPurged;
	}

	public void setTicksUntilPurged(int ticksUntilPurged)
	{
		this.ticksUntilPurged = ticksUntilPurged;
	}
	
	public void remove()
	{
		entity.remove();
	}
}
