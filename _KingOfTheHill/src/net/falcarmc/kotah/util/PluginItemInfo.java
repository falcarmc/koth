package net.falcarmc.kotah.util;

import org.bukkit.Location;

public class PluginItemInfo
{
	private int price;
	private int x;
	private int y;
	private int z;
	
	public PluginItemInfo(int x, int y, int z, int price)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.price = price;
	}

	public void setPrice(int price)
	{
		this.price = price;
	}

	public int getPrice()
	{
		return price;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getZ()
	{
		return z;
	}
}
