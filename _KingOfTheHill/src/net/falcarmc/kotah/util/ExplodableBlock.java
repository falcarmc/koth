package net.falcarmc.kotah.util;

import net.falcarmc.kotah.Main;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class ExplodableBlock
{
	private long explodeTime = -1;
	Location loc;
	private int secondsToRegen;
	private Material type;
	private byte data;
	private float distance;

	public ExplodableBlock(Location loc, Material type, byte data, int secondsToRegen, float distance)
	{
		this.loc = loc;
		this.setType(type);
		this.setData(data);
		this.setSecondsToRegen(secondsToRegen);
		this.distance = distance;
	}

	public void explode(boolean animation, long explodeTime)
	{
		if (this.explodeTime == -1)
		{
			Block block = loc.getBlock();
			if (animation)
			{
				loc.getWorld().spigot().playEffect(loc.clone().add(0.5, 0.5, 0.5), Effect.TILE_BREAK, 4, 0, 0.5F, 0.5F, 0.5F, 0.0F, 30, 30);
				new BukkitRunnable()
				{

					@Override
					public void run()
					{
						loc.getWorld().playSound(loc, Sound.DIG_STONE, 1f, 1f);
					}
				}.runTaskLater(Main.getInstance(), Main.getInstance().random.nextInt(10));
			}

			block.setType(Material.AIR);
			this.setExplodeTime(explodeTime);
		}
	}

	public void explode(boolean animation)
	{
		explode(animation, System.currentTimeMillis());
	}

	public void place()
	{
		Block block = loc.getBlock();
		block.setType(type);
		block.setData(data);
		setExplodeTime(-1);
	}

	public float getDistance()
	{
		return distance;
	}

	public Material getType()
	{
		return type;
	}

	public void setType(Material type)
	{
		this.type = type;
	}

	public byte getData()
	{
		return data;
	}

	public void setData(byte data)
	{
		this.data = data;
	}

	public long getExplodeTime()
	{
		return explodeTime;
	}

	public void setExplodeTime(long explodeTime)
	{
		this.explodeTime = explodeTime;
	}

	public int getSecondsToRegen()
	{
		return secondsToRegen;
	}

	public void setSecondsToRegen(int secondsToRegen)
	{
		this.secondsToRegen = secondsToRegen;
	}
}
