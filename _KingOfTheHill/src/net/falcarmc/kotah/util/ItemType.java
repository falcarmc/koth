package net.falcarmc.kotah.util;

public enum ItemType
{
	ROCKET_LAUNCHER, ZOMBIE_GUN, REGULAR_SWORD, SWORD, SPEED, EMPTY_SPEED;
}
