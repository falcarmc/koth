package net.falcarmc.kotah.util;

import org.bukkit.Location;

public class CrownLocation
{
	private Location loc;
	private int index;
	private int height;
	
	public CrownLocation(Location loc, int index, int height)
	{
		this.loc = loc;
		this.index = index;
		this.height = height;
	}

	public Location getLoc()
	{
		return loc;
	}

	public int getIndex()
	{
		return index;
	}

	public int getHeight()
	{
		return height;
	}
}
