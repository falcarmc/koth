package net.falcarmc.kotah;

import static com.sk89q.worldguard.bukkit.BukkitUtil.toVector;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.falcarmc.kotah.managers.BlockManager;
import net.falcarmc.kotah.managers.CommandManager;
import net.falcarmc.kotah.managers.ConfigManager;
import net.falcarmc.kotah.managers.CrownManager;
import net.falcarmc.kotah.managers.EntityManager;
import net.falcarmc.kotah.managers.GameManager;
import net.falcarmc.kotah.managers.ItemManager;
import net.falcarmc.kotah.managers.MainManager;
import net.falcarmc.kotah.managers.ManagerType;
import net.falcarmc.kotah.managers.RocketPackageManager;
import net.falcarmc.kotah.managers.ScoreManager;
import net.falcarmc.kotah.managers.ShopManager;
import net.falcarmc.kotah.managers.SpawnManager;
import net.falcarmc.kotah.managers.UserManager;
import net.falcarmc.kotah.runnables.OneTickRunnable;
import net.falcarmc.kotah.util.EnchantmentGlow;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Main extends JavaPlugin implements Listener
{
	public static Main instance;

	public static Main getInstance()
	{
		return instance;
	}

	public World mainWorld;
	// public static Manager manager;
	// public static ConfigManager cManager;
	Map<ManagerType, MainManager> managers = new HashMap<>();
	public Random random = new Random();

	public void onEnable()
	{
		instance = this;
		setupWorld();
		managers.put(ManagerType.CONFIG, new ConfigManager());
		managers.put(ManagerType.ROCKET_PACKAGE, new RocketPackageManager(mainWorld));
		managers.put(ManagerType.CROWN, new CrownManager());
		managers.put(ManagerType.GAME, new GameManager());
		managers.put(ManagerType.ITEM, new ItemManager());
		managers.put(ManagerType.SHOP, new ShopManager(mainWorld));
		managers.put(ManagerType.USER, new UserManager());
		managers.put(ManagerType.SPAWN, new SpawnManager(mainWorld));
		managers.put(ManagerType.ENTITY, new EntityManager());
		managers.put(ManagerType.COMMAND, new CommandManager());
		managers.put(ManagerType.BLOCK, new BlockManager(mainWorld));
//		managers.put(ManagerType., new );
		for(ManagerType type : managers.keySet())
		{
			Bukkit.getPluginManager().registerEvents(managers.get(type), this);
			managers.get(type).onEnable();
//			Bukkit.broadcastMessage(type.toString() + "> " + ChatColor.RED + "kingUUID is " + getGameManager().getKingUUID());
		}
		// manager = new Manager();
		// cManager = new ConfigManager();

		// new BukkitRunnable()
		// {
		// public void run()
		// {
		// }
		// }.runTaskLater(Main.plugin, 0L);
		
		registerGlow();
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new RepeatingManager(), 0L, 20L);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new OneTickRunnable(), 0L, 1L);
	}
	
	private void setupWorld()
	{
		mainWorld = Bukkit.getWorld("flatlands");
		if(mainWorld == null)
			mainWorld = Bukkit.createWorld(new WorldCreator("flatlands"));
	}

	public void onDisable()
	{
		for (ManagerType m : managers.keySet())
			managers.get(m).onDisable();
	}

	private void registerGlow()
	{
		try
		{
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		try
		{
			EnchantmentGlow glow = new EnchantmentGlow(70);
			Enchantment.registerEnchantment(glow);
		} catch (IllegalArgumentException e)
		{
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	// @EventHandler(priority = EventPriority.HIGHEST)
	// public void onPlayerDeath(PlayerDeathEvent event)
	// {
	// Player player = event.getEntity();
	// event.getDrops().clear();
	// }

	private Location worldLoc(int x, int y, int z)
	{
		return new Location(mainWorld, x, y, z);
	}

	private Location worldLoc(int x, int y, int z, float yaw, float pitch)
	{
		return new Location(mainWorld, x, y, z, yaw, pitch);
	}

	



	public ConfigManager getConfigManager()
	{
		return (ConfigManager) managers.get(ManagerType.CONFIG);
	}

	public CrownManager getCrownManager()
	{
		return (CrownManager) managers.get(ManagerType.CROWN);
	}

	public ItemManager getItemManager()
	{
		return (ItemManager) managers.get(ManagerType.ITEM);
	}

	public UserManager getUserManager()
	{
		return (UserManager) managers.get(ManagerType.USER);
	}

	public RocketPackageManager getRocketPackageManager()
	{
		return (RocketPackageManager) managers.get(ManagerType.ROCKET_PACKAGE);
	}

	public ShopManager getShopManager()
	{
		return (ShopManager) managers.get(ManagerType.SHOP);
	}

	public GameManager getGameManager()
	{
		return (GameManager) managers.get(ManagerType.GAME);
	}

	public SpawnManager getSpawnManager()
	{
		return (SpawnManager) managers.get(ManagerType.SPAWN);
	}

	public EntityManager getEntityManager()
	{
		return (EntityManager) managers.get(ManagerType.ENTITY);
	}

	public CommandManager getCommandManager()
	{
		return (CommandManager) managers.get(ManagerType.COMMAND);
	}

	public BlockManager getBlockManager()
	{
		return (BlockManager) managers.get(ManagerType.BLOCK);
	}

	public ScoreManager getScoreboardManager()
	{
		return (ScoreManager) managers.get(ManagerType.SCOREBOARD);
	}

	// @EventHandler
	// public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event)
	// {
	// Player player = event.getPlayer();
	// Entity entity = event.getRightClicked();
	// if(entity.getType() == EntityType.ARMOR_STAND)
	// {
	// ArmorStand stand = (ArmorStand) entity;
	// if(pieces.contains(stand))
	// {
	// event.setCancelled(true);
	// }
	// }
	// }

	public ApplicableRegionSet getRegionSet(Location location)
	{
		Location newLoc = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
		return WGBukkit.getRegionManager(location.getWorld()).getApplicableRegions(toVector(newLoc));
	}

	public boolean isInRegion(ApplicableRegionSet regions, String regionName)
	{
		regionName = regionName.toLowerCase();
		for (ProtectedRegion r : regions.getRegions())
		{
			String id = r.getId().toLowerCase();
			if (id.equals(regionName))
				return true;
		}
		return false;
	}
}
